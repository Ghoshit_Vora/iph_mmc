//
//  DashboardViewController.swift
//  Piano
//
//  Created by Desap Team on 13/10/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox
import AVKit


class DashboardViewController: UIViewController,GLNPianoViewDelegate,LeftMenuDelegate,CenterMenuDelegate {

    @IBOutlet var leaderboardTexts : UITextView?
    @IBOutlet var pointLabel : UILabel?
    
    let kLessonTitleKey = "lesson"
    let kLessonResultKey = "result"
    
    let kFinishStepIndex : Int = 100
    var lessonsList = NSMutableArray()
  
    //Use for current lesson array index
    var currentLessonIndex : Int = 0
    
    //Use for check keyboar hint
    var currentLessonHintIndex : Int = 0
    //Use for keyboar upcomming step
    var upcommingStepIndex : Int = 100
    
    //Current lesson seprate array
    var currentLessonList = NSArray()
    //Current lesson keyboad hint list
    var currentLessonHintResult = NSDictionary()
    
    //Check Lesson is activated
    var isLessonActivated : Bool = false
    //Finish Lesson object
    //

    @IBOutlet var lessonTitleLabel : UILabel!
    @IBOutlet var lessonFormulaLabel : UILabel!
    @IBOutlet var lessonAnswerLabel : UILabel!
    
    @IBOutlet weak var keyboardView: GLNPianoView!
    @IBOutlet var deskboardTopView : UIView?
    
    /*
    @IBOutlet weak var keyStepper: UIStepper!
    @IBOutlet weak var keyNumberLabel: UILabel!
    @IBOutlet weak var octaveStepper: UIStepper!
    @IBOutlet weak var octaveLabel: UILabel!
 */
    
    //var octave = 60
    var octave = 80
//    let audioEngine = AudioEngine()
   
    var sliderSubMenuView = CenterMenuView.instanceFromNib()
    
    var soundIDs = [SystemSoundID]()
    
    //SliderMenu
    var mainMenuActive : Bool = false
    let leftMenu = LeftMainMenu.instanceFromNib()
    let leftMenuConstant : CGFloat = 400
    
    
    @IBOutlet var leaderBoardView : UIView!
    //AlertView
    @IBOutlet var topFormulaView : UIView!
    @IBOutlet var alertMainView : UIView!
    @IBOutlet var alertIntroView : UIView!
    @IBOutlet var alertHintView : UIView!
    @IBOutlet var alertIntroMessage : UILabel!
    @IBOutlet var alertHintMessage : UILabel!
    @IBOutlet var alertIntoButtonTitle : UIButton!
    @IBOutlet var alertHintButtonTitle : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.leftMenu.delegate = self
        
//        leftMenu.temp = self
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
       
        self.topFormulaView.isHidden = true
        self.lessonTitleLabel.isHidden = true
        self.lessonFormulaLabel.isHidden = true
        self.lessonAnswerLabel.isHidden = true
        
        
        //Subject and tutorial menu
        DispatchQueue.main.async {
            self.sliderSubMenuView.frame = (self.deskboardTopView?.bounds)!
            self.sliderSubMenuView.frame.size.height = (self.deskboardTopView?.bounds.height)! - 10
            self.deskboardTopView?.addSubview(self.sliderSubMenuView)
            self.sliderSubMenuView.delegate = self
            self.sliderSubMenuView.isHidden = true
        }
    
        
        keyboardView.delegate = self;

        
        self.InsertLessonData()
    
        
        DispatchQueue.main.async {
            self.LoadPianoSound()
            self.loadSliderMenu()
        }
      
        self.setupAlertView()
        
        //MARK:- First Time Launching Video
        self.IsFirstTimeAppLaunching()
        
        //MAke api call
        self.GetDashboard()
    }
    
    func IsFirstTimeAppLaunching(){
        if let isLaucnh = UserDefaults.standard.value(forKey: "isFirstTimeLuanching") as?  Bool {
            if isLaucnh == false {
                self.PlayHelpVideo()
            }
        }else{
            UserDefaults.standard.set(true, forKey: "isFirstTimeLuanching")
            UserDefaults.standard.synchronize()
            self.PlayHelpVideo()
        }
    }
    func PlayHelpVideo(){
        // Movie player
        if let videoUrl = URL(string: "http://www.makemusiccount.online/uploads/videos/Blackwell19REV.mp4") {
            let player = AVPlayer(url: videoUrl)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
        
    }
    func setupAlertView(){
        self.alertHintView.backgroundColor = UIColor(white: 255, alpha: 0.8)
        self.alertIntroView.backgroundColor = UIColor(white: 255, alpha: 0.8)
        self.alertMainView.backgroundColor = UIColor.clear
        self.alertMainView.frame = UIScreen.main.bounds
       
        
        self.view.addSubview(self.alertMainView)
        
        self.alertMainView.isHidden = true
        
    }
    func LoadPianoSound(){
        
        if let path = Bundle.main.path(forResource: "keyboardLayout", ofType: "plist") {
            
            //If your plist contain root as Array
            if let array = NSArray(contentsOfFile: path) {
                for (_, result) in array.enumerated() {
                    
                    if let pathStr = result as? String {
                        let sndurl = Bundle.main.url(forResource:pathStr, withExtension: "wav")!
                        var SoundID : SystemSoundID = 0
                        AudioServicesCreateSystemSoundID(sndurl as CFURL, &SoundID)
                        /*
                         // watch _this_ little move
                         AudioServicesAddSystemSoundCompletion(SoundID, nil, nil, {
                         sound, context in
                         print("finished!")
                         AudioServicesRemoveSystemSoundCompletion(sound)
                         AudioServicesDisposeSystemSoundID(sound)
                         }, nil)
                         */
                        self.soundIDs.append(SoundID)
                    }
                    
                }
            }
            
        }
        
        
        
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
    }
    //MARK:- Start Lesson Theory -
    //Refresh Lesson
    func refreshLesson(){
        self.alertHintView.isHidden = true
        self.alertIntroView.isHidden = true
        self.alertMainView.isHidden = true
        self.lessonFormulaLabel.isHidden = true
        self.lessonAnswerLabel.isHidden = true
        self.lessonTitleLabel.isHidden = true
        self.upcommingStepIndex = self.kFinishStepIndex
        self.currentLessonHintIndex = 0
        self.currentLessonIndex = 0
        self.currentLessonHintResult = NSDictionary()
        self.currentLessonList = NSArray()
        self.lessonTitleLabel.isHidden = true
        self.lessonAnswerLabel.isHidden = true
        self.isLessonActivated = false
        self.leaderBoardView.isHidden = false
        self.topFormulaView.isHidden = true
    }
    func InitLesson(index : Int){
    
        if self.lessonsList.count > index {
            if let result = self.lessonsList.object(at: index) as? NSDictionary {
                if let topTitle = result.object(forKey: kLessonTitleKey) as? String {
                    print("Lesson Title\(topTitle)")
                    self.lessonTitleLabel.text = topTitle
                    self.lessonTitleLabel.isHidden = false
                }
                if let lessons = result.object(forKey: kLessonResultKey) as? NSArray {
                    self.currentLessonList = NSArray(array: lessons)
                }
                
                self.startLesson()

            }
        }
       
    }
    func startLesson(){
        if self.currentLessonList.count > self.currentLessonIndex {
            if let result = self.currentLessonList.object(at: self.currentLessonIndex) as? NSDictionary {
                if let lessonType = result.object(forKey: kLessonType) as? String {
                    if lessonType.caseInsensitiveCompare(kLessonTypeIntro) == .orderedSame {
                        var message = ""
                        var buttonTitle = ""
                        if let valueStr = result.object(forKey: kLessonDesc) as? String {
                            message = valueStr
                        }
                        if let valueStr = result.object(forKey: kLessonButton) as? String {
                            buttonTitle = valueStr
                        }
                        
                        if self.checkHintButtonActive() == true {
                            self.showHintMessage(message: message, buttonTitle: buttonTitle)
                        }else{
                            self.showIntroMessage(message: message, buttonTitle: buttonTitle)
                        }
                        
                    }else{
                        self.currentLessonHintResult = NSDictionary(dictionary: result)
                        print(self.currentLessonHintResult)
                        if let keysList = self.currentLessonHintResult.object(forKey: kLessonHint) as? NSArray {
                            if keysList.count > self.currentLessonHintIndex {
                                if let keyNumber = keysList.object(at: self.currentLessonHintIndex) as? NSNumber {
                                    self.upcommingStepIndex = keyNumber.intValue
                                    self.keyboardView.startPulse(keyNum: keyNumber.intValue)
                                    self.isLessonActivated = true
                                }
                            }
                            
                        }
                    }
                }
            }
        }
    }
    func checkHintButtonActive() -> Bool {
        var isHintActive : Bool = false
        if self.currentLessonList.count > self.currentLessonIndex + 1 {
            if let result = self.currentLessonList.object(at: self.currentLessonIndex + 1) as? NSDictionary {
                if let lessonType = result.object(forKey: kLessonType) as? String {
                    if lessonType.caseInsensitiveCompare(kLessonTypeFormula) == .orderedSame {
                        if let valueStr = result.object(forKey: kLessonFormula) as? String {
                            self.lessonFormulaLabel.text = String(format: "%@", arguments: [valueStr])
                            self.lessonFormulaLabel.isHidden = false
                            isHintActive = true
                        }

                    }
                }
            }
        }
        return isHintActive
    }
    
    //MARK:- Custom Alert -
    func showIntroMessage(message : String, buttonTitle : String){
        self.alertHintView.isHidden = true
        self.alertIntroView.isHidden = false
        self.alertMainView.isHidden = false
        self.alertIntoButtonTitle.setTitle(buttonTitle, for: .normal)
        self.alertIntroMessage.text = message
        
    }
    @IBAction func intoButtonClicked(){
        self.currentLessonIndex = self.currentLessonIndex + 1
        self.startLesson()
    }
    func showHintMessage(message : String, buttonTitle : String){
        self.alertHintView.isHidden = false
        self.alertIntroView.isHidden = true
        self.alertMainView.isHidden = false
        self.topFormulaView.isHidden = false
        self.alertHintButtonTitle.setTitle(buttonTitle, for: .normal)
        self.alertHintMessage.text = message
        
    }
    @IBAction func hintButtonClicked(){
        self.alertMainView.isHidden = true
        self.currentLessonIndex = self.currentLessonIndex + 1
        self.startLesson()
    }
   
   
   
    
    func stopAllPulase(){
        for (index,value) in keyboardView.keyLayers.enumerated() {
            if let layer = value as? DesapLayer {
                layer.nextStep = 0
                
                keyboardView.stopPulse(keyNum: index)
            }
        }
    }
   
    
    //MARK:- KeyBoard Delegate -
    func pianoKeyUp(_ keyNumber:Int) {
        print("pianoKeyUp")
        print(keyNumber)
//        audioEngine.sampler.stopNote(UInt8(octave + keyNumber), onChannel: 0)
        
        keyboardView.stopPulse(keyNum: keyNumber)
//        self.pulseNextStep(keyNumber: keyNumber)
    }
    //Manage Keyboard
    func pianoKeyDown(_ keyNumber:Int) {
        print("pianoKeyDown")
        print(keyNumber)
        //        audioEngine.sampler.startNote(UInt8(octave + keyNumber), withVelocity: 64, onChannel: 0)
        //        audioEngine.sampler.startNote(UInt8(octave + keyNumber), withVelocity: 64, onChannel: 0)
        
//        self.stopAllSounds()
        
        
        if self.soundIDs.count > keyNumber {
            DispatchQueue.main.async {
                AudioServicesPlaySystemSound(self.soundIDs[keyNumber])
            }
            
        }
        
        
        keyboardView.stopPulse(keyNum: keyNumber)
        
        DispatchQueue.main.async {
            self.CheckLessonHintAnswer(keyNumber: keyNumber)
        }
        
    }
    
    //MARK:- Check Hint Number -
    func CheckLessonHintAnswer(keyNumber : Int){
        if self.upcommingStepIndex == keyNumber {
            //Check ANy New Hints found
            if let keysList = self.currentLessonHintResult.object(forKey: kLessonHint) as? NSArray {
                self.currentLessonHintIndex = self.currentLessonHintIndex + 1
                if keysList.count > self.currentLessonHintIndex {
                    if let keyNumber = keysList.object(at: self.currentLessonHintIndex) as? NSNumber {
                        self.upcommingStepIndex = keyNumber.intValue
                        self.keyboardView.startPulse(keyNum: keyNumber.intValue)
                    }
                }else{
                    if let valueStr = self.currentLessonHintResult.object(forKey: kLessonAnswer) as? String {
                        self.upcommingStepIndex = self.kFinishStepIndex
                        self.lessonAnswerLabel.text = valueStr
                        self.lessonAnswerLabel.isHidden = false
                        
                    }
                }
                
            }
        }else{
            if self.isLessonActivated == true {
                if self.upcommingStepIndex != self.kFinishStepIndex {
                    self.lessonAnswerLabel.text = "Wrong"
                    self.lessonAnswerLabel.isHidden = false
                }
                
            }
        }
    }
    
    func stopAllSounds(){
        for soundId in self.soundIDs {
            AudioServicesDisposeSystemSoundID(soundId)
            
        }
    }
    @IBAction func startLessonClick(){
        let controller = PlaySongsViewController(nibName: "PlaySongsViewController", bundle: nil)
        controller.isSongMenuActivated = false
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func keyStepperTapped(_ sender: UIStepper) {
        keyboardView.setNumberOfKeys(count: Int(sender.value))
//        keyNumberLabel.text = String(keyStepper.value)
    }
    
    @IBAction func octaveStepperTapped(_ sender: UIStepper) {
        octave = Int(sender.value)
//        octaveLabel.text = String(octave)
    }
    
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:- Load Lessons -
    func InsertLessonData(){
        self.lessonsList = NSMutableArray()
        
        
        //1 Step -  //welcome
        //index = 0
        self.lessonsList.add([kLessonTitleKey : "Half Steps", kLessonResultKey : halfStepsFinalResult])
        self.lessonsList.add([kLessonTitleKey : "Whole Steps", kLessonResultKey : wholeStepsFinalResult])
        self.lessonsList.add([kLessonTitleKey : "StepsMajor Scales", kLessonResultKey : MajorStepsFinalResult])
        self.lessonsList.add([kLessonTitleKey : "Warm Up Songs", kLessonResultKey : WarmUpStepsFinalResult])
        self.lessonsList.add([kLessonTitleKey : "Substituting Variables", kLessonResultKey : SubstitutingStepsFinalResult])
        self.lessonsList.add([kLessonTitleKey : "Solve for X", kLessonResultKey : SolveforXFinalResult])
        self.lessonsList.add([kLessonTitleKey : "Creating Algebra Equations", kLessonResultKey : AlgebraStepsFinalResult])
        
    }
    /*
    // MARK: - Navigation

     
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- SliderMenu -
    func loadSliderMenu(){
        
        
        
        var frame = self.view.bounds
        frame.origin.x = -self.leftMenuConstant
        frame.size.width = self.leftMenuConstant
        self.leftMenu.frame = frame
        self.view.addSubview(leftMenu)
        
        
    }
    @IBAction func openMainMenu(_ sender: Any) {
        var frame = self.leftMenu.frame
        if (self.mainMenuActive) {
            frame.origin.x = -self.leftMenuConstant
        } else {
            frame.origin.x = 0
           
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.leftMenu.frame = frame
        })
        
        self.sliderSubMenuView.isHidden = true
       
        
        self.mainMenuActive = !mainMenuActive
        
        self.topFormulaView.isHidden = true
        self.lessonTitleLabel.text = ""
    }
    //MARK:- SliderMenu Delegate -
    func lessonStartIndex(index: Int) {
        
        self.stopAllPulase()
        self.refreshLesson()
        
        if index == 0 {
            //Half step
            self.InitLesson(index: index)
        }else if index == 1 {
            self.InitLesson(index: index)
        }else if index == 2 {
            self.InitLesson(index: index)
        }else if index == 3 {
            self.InitLesson(index: index)
        }else if index == 4 {
            self.InitLesson(index: index)
        }else if index == 5 {
            self.InitLesson(index: index)
        }else if index == 6 {
            self.InitLesson(index: index)
        }
        
        if index != kFinishStepIndex {
            self.hideMenuPressed()
        }
        
    }
    func didSelectMainMenu(menu: String) {
        self.sliderSubMenuView.isHidden = true
        
        self.hideMenuPressed()
        
        if menu.caseInsensitiveCompare("songs") == .orderedSame {
            let controller = PlaySongsViewController(nibName: "PlaySongsViewController", bundle: nil)
            controller.isSongMenuActivated = false
            self.navigationController?.pushViewController(controller, animated: true)
//            let controller = SongsViewController(nibName: "SongsViewController", bundle: nil)
//            self.navigationController?.pushViewController(controller, animated: true)
        }else if menu.caseInsensitiveCompare("sign out") == .orderedSame || menu.caseInsensitiveCompare("login") == .orderedSame {
            self.hideMenuPressed()
            removeUserAuthentication()
            let controller = LoginViewController(nibName: "LoginViewController", bundle: nil)
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        else if menu.caseInsensitiveCompare("subjects") == .orderedSame {
            
            let controller = SubjectViewController(nibName: "SubjectViewController", bundle: nil)
            self.navigationController?.pushViewController(controller, animated: true)
            /*
            let subjects : NSArray = [["title":"Warm Up Songs"],["title":"Substituting Variables"],["title":"Solve for X"],["title":"Creating Algebra Equations"]]
            self.sliderSubMenuView.listOfMenus = subjects
            self.sliderSubMenuView.registerCell()
            self.sliderSubMenuView.isHidden = false
            self.sliderSubMenuView.activeMenuName = "subjects"
             */
            
        }else if menu.caseInsensitiveCompare("my profile") == .orderedSame {
            let controller = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
            self.navigationController?.pushViewController(controller, animated: true)
        }else if menu.caseInsensitiveCompare("leaderboard") == .orderedSame {
            let controller = LeaderboardViewController(nibName: "LeaderboardViewController", bundle: nil)
            self.navigationController?.pushViewController(controller, animated: true)
        }
            
        else if menu.caseInsensitiveCompare("tutorial") == .orderedSame {
//            let tutorials : NSArray = [["title":"Half Steps"],["title":"Whole Steps"],["title":"Major Scales"]]
//            self.sliderSubMenuView.activeMenuName = "tutorial"
//            self.sliderSubMenuView.listOfMenus = tutorials
//            self.sliderSubMenuView.registerCell()
//            self.sliderSubMenuView.isHidden = false
            
            let controller = TutorialViewController(nibName: "TutorialViewController", bundle: nil)
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    func hideMenuPressed() {
        var frame = self.leftMenu.frame
        frame.origin.x = -self.leftMenuConstant
        UIView.animate(withDuration: 0.3, animations: {
            self.leftMenu.frame = frame
        })
        self.mainMenuActive = false
    }
    
    //MARK:- Subject and TutorialMenuClick Events -
    func didSelectItemAtIndex(index : Int, menu : String) {
        self.sliderSubMenuView.isHidden = true
        if menu == "subjects" {
            self.lessonStartIndex(index: index + 3)
        }else{
            self.lessonStartIndex(index: index)
        }
    }
    @IBAction func backClick(){
        self.sliderSubMenuView.isHidden = false
    }
    @IBAction func subjectMenuClick(sender : UIButton){
        
        self.lessonStartIndex(index: sender.tag + 3)
    }
    @IBAction func tutorialMenuClick(sender : UIButton){
        
        self.lessonStartIndex(index: sender.tag)
    }
    //Mark - Touch Handling Code -
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if self.mainMenuActive == true {
            var frame = self.leftMenu.frame
            frame.origin.x = -self.leftMenuConstant
            UIView.animate(withDuration: 0.3, animations: {
                self.leftMenu.frame = frame
            })
            self.mainMenuActive = false
        }
        
    }
    
    //MARK:- Deskboard api -
    //MARK:- Make API Call -
    func GetDashboard(){
        
        
        
        var userId = ""
        if let idStr = getUserAuthentication().object(forKey: "userID") as? String {
            userId = idStr
        }
        
        let requestUrl = String(format: "%@?view=dashboard&userID=%@&fcmID=1&deviceID=1&versionID=1", arguments: [kRequestDomain,userId])
        
        
        print(requestUrl)
        appDelegate.DesapShowLoader()
        request(requestUrl, headers: nil).responseJSON { response in
            print(response)
            appDelegate.DesapHideLoader()
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
                if let jsonRes = json as? NSDictionary {
                    if let statusCode = jsonRes.object(forKey: "msgcode") as? String {
                        if statusCode == "0" {
                            if let leaderboard = jsonRes.object(forKey: "leaderboard") as? NSArray {
                                var leaderBoardString = ""
                                for leader in leaderboard {
                                    if let boardData = leader as? NSDictionary {
                                        
                                        if let tempStr = boardData.object(forKey: "name") as? String {
                                            leaderBoardString = leaderBoardString.appendingFormat("%@", tempStr)
                                        }
                                        if let tempStr = boardData.object(forKey: "point") as? String {
                                           
                                            leaderBoardString = leaderBoardString.appendingFormat("\n%@\n_____________\n", tempStr)
                                        }
                                        
                                        
                                    }
                                }
                                self.leaderboardTexts?.text = leaderBoardString
                            }
                            if let point = jsonRes.object(forKey: "point") as? String {
                                self.pointLabel?.text = point
                            }
                            //
                            
                        }else{
                            if let message = jsonRes.object(forKey: "message") as? String {
                                appDelegate.TNMShowMessage("", message: message)
                                //                                JYToast().isShow(message)
                            }
                        }
                    }
                }else{
                    JYToast().isShow(kErrorMessage)
                }
            }else{
                JYToast().isShow(kErrorMessage)
            }
        }
    }


}
