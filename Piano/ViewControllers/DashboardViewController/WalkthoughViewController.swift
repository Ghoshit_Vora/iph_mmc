//
//  WalkthoughViewController.swift
//  Piano
//
//  Created by Desap Team on 13/11/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit

class WalkthoughViewController: UIViewController {

    @IBOutlet var firstView : UIView!
    @IBOutlet var secondView : UIView!
    @IBOutlet var thirdView : UIView!
    @IBOutlet var fourView : UIView!
    @IBOutlet var fiveView : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.firstView.frame = UIScreen.main.bounds
        self.view.addSubview(self.firstView)
    }
    @IBAction func firstNextClick(){
        self.secondView.frame = UIScreen.main.bounds
        self.view.addSubview(self.secondView)
        self.firstView.removeFromSuperview()
    }
    @IBAction func firstExitClick(){
        self.ExitTutoria()
    }
    @IBAction func secondNextClick(){
        self.thirdView.frame = UIScreen.main.bounds
        self.view.addSubview(self.thirdView)
        self.secondView.removeFromSuperview()
    }
    @IBAction func secondExitClick(){
        self.ExitTutoria()
    }
    @IBAction func thirdNextClick(){
        self.fourView.frame = UIScreen.main.bounds
        self.view.addSubview(self.fourView)
        self.thirdView.removeFromSuperview()
    }
    @IBAction func thirdExitClick(){
        self.ExitTutoria()
    }
    @IBAction func fourNextClick(){
        self.fiveView.frame = UIScreen.main.bounds
        self.view.addSubview(self.fiveView)
        self.fourView.removeFromSuperview()
    }
    @IBAction func fourExitClick(){
        self.ExitTutoria()
    }
    @IBAction func fiveNextClick(){
        
    }
    @IBAction func fiveExitClick(){
        self.ExitTutoria()
    }
    

    func ExitTutoria(){
        let controller = DashboardViewController(nibName: "DashboardViewController", bundle: nil)
        appDelegate.navigationController = TNMNavigationDefault(rootViewController: controller)
        appDelegate.window?.rootViewController = appDelegate.navigationController
//        self.navigationController?.pushViewController(controller, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
