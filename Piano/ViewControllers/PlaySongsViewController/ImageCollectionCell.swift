//
//  ImageCollectionCell.swift
//  Piano
//
//  Created by Desap Team on 14/03/2018.
//  Copyright © 2018 Piano. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {

    @IBOutlet var imageview : UIImageView?
    var data = NSDictionary(){
        didSet{
            configureUI()
        }
    }
    func configureUI(){
        if self.imageview != nil {
            self.updateUI()
        }else{
            self.perform(#selector(updateUI), with: nil, afterDelay: 0.2)
        }
    }
    @objc func updateUI(){
        if let imageUrl = self.data.object(forKey: "image") as? String {
            if let request = URL(string: imageUrl) {
                self.imageview?.af_setImage(withURL: request, placeholderImage: #imageLiteral(resourceName: "userDefault"), filter: nil, progress: nil, progressQueue: .main, imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: true, completion: { (response) in
                    
                })
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
