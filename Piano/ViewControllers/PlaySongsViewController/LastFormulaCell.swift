//
//  FormulaCell.swift
//  Piano
//
//  Created by Desap Team on 08/12/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit
import AVFoundation

protocol LastFormulaCellDelegate {
    func backToSongMenu()
    func TestQuestionOption()
    func TestQuestionAnswerPress(isRight : Bool)
}
class LastFormulaCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionview : UICollectionView?
    
    var delegate : LastFormulaCellDelegate?
    var image_list = NSArray()
    
    @IBOutlet var titleLabel : UILabel!
    var bombSoundEffect: AVAudioPlayer?
    var result = NSDictionary() {
        didSet{
            updateUI()
        }
    }
    
    @IBOutlet var image1 : UIImageView?
    @IBOutlet var image2 : UIImageView?
    @IBOutlet var image3 : UIImageView?
    @IBOutlet var image4 : UIImageView?
    
    @IBOutlet var playButton : UIButton?
    
    var isPlayingSong : Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        self.setSongImage()
    }

    func updateUI(){
        print(result)
        if collectionview != nil {
            self.perform(#selector(setSongImage), with: nil, afterDelay: 0.1)
        }else{
            self.perform(#selector(setSongImage), with: nil, afterDelay: 0.1)
        }
        
    }
    
    @IBAction func playSong(){
        
        if self.isPlayingSong == true {
            self.isPlayingSong = false
            Peep.stop()
            self.playButton?.setTitle("Play", for: .normal)
        }else{
            if let song_file = self.result.object(forKey: "song_file") as? String {
                if song_file.isEmpty == false {
                    if let mp3Url = URL(string: song_file) {
                        self.isPlayingSong = true
                        Peep.play(sound: mp3Url)
                        self.playButton?.setTitle("Stop", for: .normal)
                    }
                }
                
            }
        }
        
        
    }
    @IBAction func testQuestionClick(){
        Peep.stop()
        if self.delegate != nil {
            self.delegate?.TestQuestionOption()
        }
    }
    @IBAction func backTSongClick(){
        Peep.stop()
        if self.delegate != nil {
            self.delegate?.backToSongMenu()
        }
    }
    @objc func setSongImage(){
         if let images = self.result.object(forKey: "image_list") as? NSArray {
            self.image_list = NSArray(array: images)
        }
        self.CollectionViewUpdate()
       
    }
    func CollectionViewUpdate(){
        
        let nibName = UINib(nibName: "ImageCollectionCell", bundle:nil)
        self.collectionview?.register(nibName, forCellWithReuseIdentifier: "ImageCollectionCell")
        
        self.collectionview?.dataSource = self
        self.collectionview?.delegate = self
        self.collectionview?.reloadData()
    }
    
    //MARK: - UICollectionView Delegate -
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.size.width / 4
        return CGSize(width: width, height: collectionView.bounds.size.height);
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.image_list.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as? ImageCollectionCell {
            if self.image_list.count > indexPath.row {
                if let result = self.image_list.object(at: indexPath.row) as? NSDictionary {
                    cell.data = result
                    return cell
                }
            }
        }
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
    }
}
