//
//  SongListCell.swift
//  Piano
//
//  Created by Desap Team on 08/12/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit

class SongListCell: UICollectionViewCell {

    @IBOutlet var playButtonTitle : UILabel?
    @IBOutlet var songBackground : UIImageView?
    
    @IBOutlet var songImage : UIImageView!
    @IBOutlet var songName : UILabel!
    var data = NSDictionary()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.songBackground?.layer.cornerRadius = 15
        self.songBackground?.clipsToBounds = true
        
    }

}
