//
//  FormulaCell.swift
//  Piano
//
//  Created by Desap Team on 08/12/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit

class FormulaCell: UICollectionViewCell {

    @IBOutlet var titleLabel : UILabel!
    @IBOutlet var formulaLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
