//
//  FormulaCell.swift
//  Piano
//
//  Created by Desap Team on 08/12/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit
import AVFoundation


class TestQuestionCell: UICollectionViewCell {

     var delegate : LastFormulaCellDelegate?
    
    @IBOutlet var answer1Label : UIButton?
    @IBOutlet var answer2Label : UIButton?
    @IBOutlet var answer3Label : UIButton?
    
    @IBOutlet var testQuestionLabel : UILabel?
    
    var result = NSDictionary() {
        didSet{
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    func updateUI(){
        print(result)
        if self.testQuestionLabel != nil {
            self.perform(#selector(setUIResult), with: nil, afterDelay: 0.1)
        }else{
            self.perform(#selector(setUIResult), with: nil, afterDelay: 0.1)
        }
        
    }
    @objc func setUIResult(){
        if let tempStr = self.result.object(forKey: "question") as? String {
            self.testQuestionLabel?.text = tempStr
        }
        if let tempStr = self.result.object(forKey: "ans_1") as? String {
            self.answer1Label?.setTitle(tempStr, for: .normal)
        }
        if let tempStr = self.result.object(forKey: "ans_2") as? String {
            self.answer2Label?.setTitle(tempStr, for: .normal)
        }
        if let tempStr = self.result.object(forKey: "ans_3") as? String {
            self.answer3Label?.setTitle(tempStr, for: .normal)
        }
    }
    
    @IBAction func Answer1Click(){
        var right_ans = ""
        if let tempStr = self.result.object(forKey: "right_ans") as? String {
            right_ans = tempStr
        }
        var ans_1 = ""
        if let tempStr = self.result.object(forKey: "ans_1") as? String {
            ans_1 = tempStr
        }
        if right_ans == ans_1 {
            self.answer1Label?.setImage(#imageLiteral(resourceName: "rightAns"), for: .normal)
            if self.delegate != nil {
                self.delegate?.TestQuestionAnswerPress(isRight: true)
            }
        }else{
            self.answer1Label?.setImage(#imageLiteral(resourceName: "wrongAns"), for: .normal)
            if self.delegate != nil {
                self.delegate?.TestQuestionAnswerPress(isRight: false)
            }
            
            
        }
    }
    @IBAction func Answer2Click(){
        var right_ans = ""
        if let tempStr = self.result.object(forKey: "right_ans") as? String {
            right_ans = tempStr
        }
        var ans_2 = ""
        if let tempStr = self.result.object(forKey: "ans_2") as? String {
            ans_2 = tempStr
        }
        if right_ans == ans_2 {
            self.answer2Label?.setImage(#imageLiteral(resourceName: "rightAns"), for: .normal)
            if self.delegate != nil {
                self.delegate?.TestQuestionAnswerPress(isRight: true)
            }
        }else{
            self.answer2Label?.setImage(#imageLiteral(resourceName: "wrongAns"), for: .normal)
            if self.delegate != nil {
                self.delegate?.TestQuestionAnswerPress(isRight: false)
            }
        }
    }
    @IBAction func Answer3Click(){
        var right_ans = ""
        if let tempStr = self.result.object(forKey: "right_ans") as? String {
            right_ans = tempStr
        }
        var ans_3 = ""
        if let tempStr = self.result.object(forKey: "ans_3") as? String {
            ans_3 = tempStr
        }
        if right_ans == ans_3 {
            self.answer3Label?.setImage(#imageLiteral(resourceName: "rightAns"), for: .normal)
            if self.delegate != nil {
                self.delegate?.TestQuestionAnswerPress(isRight: true)
            }
        }else{
            self.answer3Label?.setImage(#imageLiteral(resourceName: "wrongAns"), for: .normal)
            if self.delegate != nil {
                self.delegate?.TestQuestionAnswerPress(isRight: false)
            }
        }
    }
    
    
    
    @IBAction func backToSongClick(){
        if self.delegate != nil {
            self.delegate?.backToSongMenu()
        }
    }
    
}
