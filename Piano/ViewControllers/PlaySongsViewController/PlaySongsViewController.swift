//
//  SongsViewController.swift
//  Piano
//
//  Created by Desap Team on 07/12/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox

class PlaySongsViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,GLNPianoViewDelegate,LastFormulaCellDelegate {
    @IBOutlet var collectionView : UICollectionView!
    
    @IBOutlet var finishSongView : UIView?
    @IBOutlet var pointLabel : UILabel?
    @IBOutlet var headingLabel : UILabel?
    
    var isTestQuestionActivated : Bool = false
    
    var isSongMenuActivated : Bool = true
    
    var isLastSongActivated : Bool = false
    
    var formulaCurrentIndex : Int = 0
    
    var activeMenuName = "lessons"
    
    var activeSongResult = NSDictionary()
    
    var listOfSongsInLive = NSArray()
//    let listOfSongs : NSArray = [["title":"sound1","image":#imageLiteral(resourceName: "sound1")],["title":"sound2","image":#imageLiteral(resourceName: "sound2")],["title":"sound3","image":#imageLiteral(resourceName: "sound3")],["title":"sound4","image":#imageLiteral(resourceName: "sound4")],["title":"sound5","image":#imageLiteral(resourceName: "sound5")],["title":"sound5","image":#imageLiteral(resourceName: "sound5")],["title":"sound6","image":#imageLiteral(resourceName: "sound6")]]

    var listOfFormulas = NSMutableArray()
    var listOfTestQuestions = NSMutableArray()
    
    var songCompaleteResult = NSDictionary()
    
//    let listOfFormulas : NSArray = [["title" : "flex\nrich homie quad","formula" : "F# + 1 = G#"],["title" : "flex\nrich homie quad","formula" : "G# + 1/2 = _______"],["title" : "flex\nrich homie quad","formula" : "G# + 1/2 = B"],["title" : "flex\nrich homie quad","formula" : "B - 4 = _______"],["title" : "flex\nrich homie quad","formula" : "B - 4 = E"],["title" : "flex\nrich homie quad","formula" : "F# | G# | B | E"]]
    
    
    @IBOutlet var popupsView : UIView?
     @IBOutlet var popupsImageContainer : UIView?
    @IBOutlet var popupsImage : UIImageView?
    @IBOutlet var messageLabel : UILabel?
     @IBOutlet var messageContainer : UIView?
    
    @IBOutlet var prevButton : UIButton?
    @IBOutlet var nextButton : UIButton?
    @IBOutlet var pageController : UIPageControl?
    
    var catID = ""
    var subcateID = ""
    
    
    
    var soundIDs = [SystemSoundID]()
    @IBOutlet weak var keyboardView: GLNPianoView! {
        didSet{
            keyboardView.delegate = self
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        
        
        self.pageController?.isHidden = true
        self.GetSongList()
        self.finishSongView?.isHidden = true
        
        DispatchQueue.main.async {
            self.setupAlertView()
            self.LoadPianoSound()
        }
    }
    
    func setupAlertView(){
//        self.popupsImageContainer?.backgroundColor = UIColor(white: 255, alpha: 0.8)
       
        self.messageContainer?.layer.borderColor = UIColor(red: 195/255, green: 223/255, blue: 92/255, alpha: 1.0).cgColor
        self.messageContainer?.layer.cornerRadius = 10
        self.messageContainer?.layer.borderWidth = 2
        self.popupsView?.frame = UIScreen.main.bounds
        
        self.view.addSubview(self.popupsView!)
        
        self.popupsView?.isHidden = true
        
    }
    func LoadPianoSound(){
        
        if let path = Bundle.main.path(forResource: "keyboardLayout", ofType: "plist") {
            
            //If your plist contain root as Array
            if let array = NSArray(contentsOfFile: path) {
                for (_, result) in array.enumerated() {
                    
                    if let pathStr = result as? String {
                        let sndurl = Bundle.main.url(forResource:pathStr, withExtension: "wav")!
                        var SoundID : SystemSoundID = 0
                        AudioServicesCreateSystemSoundID(sndurl as CFURL, &SoundID)
                        /*
                         // watch _this_ little move
                         AudioServicesAddSystemSoundCompletion(SoundID, nil, nil, {
                         sound, context in
                         print("finished!")
                         AudioServicesRemoveSystemSoundCompletion(sound)
                         AudioServicesDisposeSystemSoundID(sound)
                         }, nil)
                         */
                        self.soundIDs.append(SoundID)
                    }
                    
                }
            }
            
        }
        
        
    }
    

    func CollectionViewUpdate(){
        
        let nibName = UINib(nibName: "SongListCell", bundle:nil)
        self.collectionView.register(nibName, forCellWithReuseIdentifier: "SongListCell")
        
        let nibName1 = UINib(nibName: "FormulaCell", bundle:nil)
        self.collectionView.register(nibName1, forCellWithReuseIdentifier: "FormulaCell")
        
        let nibName2 = UINib(nibName: "FormulaAnsCell", bundle:nil)
        self.collectionView.register(nibName2, forCellWithReuseIdentifier: "FormulaAnsCell")
        
        let nibName3 = UINib(nibName: "LastFormulaCell", bundle:nil)
        self.collectionView.register(nibName3, forCellWithReuseIdentifier: "LastFormulaCell")
        
        let nibName4 = UINib(nibName: "TestQuestionCell", bundle:nil)
        self.collectionView.register(nibName4, forCellWithReuseIdentifier: "TestQuestionCell")
        
        
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.reloadData()
    }
    @IBAction func optionClick() {
        if self.activeMenuName == "lessons" {
            self.activeMenuName = "songs"
            #imageLiteral(resourceName: "songs2")
        }else if self.activeMenuName == "songs" {
            self.activeMenuName = "play"
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        Peep.stop()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func openMainMenu(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func hintKeyClick(){
        if self.listOfFormulas.count > self.formulaCurrentIndex {
            if let result = self.listOfFormulas.object(at: self.formulaCurrentIndex) as? NSDictionary {
                if let answer = result.object(forKey: "key_value") as? String {
                    if let ke_value = Int(answer) {
                        var pulseKey = ke_value - 1
                        if pulseKey < 0 {
                            pulseKey = 0
                        }
                        self.keyboardView.startPulse(keyNum: pulseKey)
                    }
                }
            }
        }
    }
    //MARK: - UICollectionView Delegate -
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.isSongMenuActivated == true {
            return CGSize(width: collectionView.bounds.size.width / 3, height: collectionView.bounds.size.width / 3);
        }else{
            return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height);
        }
        
    }

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.isSongMenuActivated == true {
            return self.listOfSongsInLive.count
        }else if self.isTestQuestionActivated == true {
            return self.listOfTestQuestions.count
        }
        else{
            return self.listOfFormulas.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        self.finishSongView?.isHidden = true
        
        if self.isSongMenuActivated == true {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SongListCell", for: indexPath) as? SongListCell {
                if self.listOfSongsInLive.count > indexPath.row {
                    if let result = self.listOfSongsInLive.object(at: indexPath.row) as? NSDictionary {
                        cell.data = result
                        
                        if let status = result.object(forKey: "status") as? String {
                            if status.caseInsensitiveCompare("Active") == .orderedSame {
//                                cell.songBackground?.image = #imageLiteral(resourceName: "rect-box")
                                cell.songBackground?.image = nil
                                
                                cell.songBackground?.backgroundColor = NSTheme().GetActiveSongBG()
                                cell.playButtonTitle?.text = "Play"
                                cell.songName.textColor = UIColor.white
                                cell.playButtonTitle?.textColor = NSTheme().GetSongButtonTextColor()
                            }else{
                                cell.songBackground?.image = #imageLiteral(resourceName: "sound2")
                                cell.playButtonTitle?.text = "Locked"
                                cell.songName.textColor = UIColor.darkGray
                                cell.playButtonTitle?.textColor = UIColor.darkGray
                            }
                        }
                        
                        if let imageUrl = result.object(forKey: "image") as? String {
                            if let request = URL(string: imageUrl) {
                                cell.songImage.af_setImage(withURL: request, placeholderImage: #imageLiteral(resourceName: "userDefault"), filter: nil, progress: nil, progressQueue: .main, imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: true, completion: { (response) in
                                    
                                    if let image = response.result.value {
                                        if image.size.width + 5 < image.size.height {
                                            cell.songImage.contentMode = .scaleAspectFit
                                        }
                                        
                                    }
                                    
                                })
                            }
                            
                        }
                        if let name = result.object(forKey: "name") as? String {
                            cell.songName.text = name
                        }
                        
                    }
                }
                return cell
            }
        }else if self.isTestQuestionActivated == true {
            if self.listOfTestQuestions.count > indexPath.row {
                if let result = self.listOfTestQuestions.object(at: indexPath.row) as? NSDictionary {
                    if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TestQuestionCell", for: indexPath) as? TestQuestionCell {
                        cell.delegate = self
                        cell.result = result
                        return cell
                    }
                }
            }
            
        }
        else{
            
            if self.listOfFormulas.count > indexPath.row {
                if let result = self.listOfFormulas.object(at: indexPath.row) as? NSDictionary {
                    
                    if self.isLastSongActivated == true && self.listOfFormulas.count == 1 {
                        
                        self.finishSongView?.isHidden = false
                        if let tempStr = result.object(forKey: "point_text") as? String {
                           self.pointLabel?.text = tempStr
                        }
                        if let tempStr = result.object(forKey: "heading") as? String {
                            self.headingLabel?.text = tempStr
                        }
                        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LastFormulaCell", for: indexPath) as? LastFormulaCell {
                            cell.delegate = self
                            if let songTitle = result.object(forKey: "song_name") as? String {
                                cell.titleLabel.text = songTitle
                                
                                cell.result = result
                            }
                             return cell
                        }
                    }else{
                        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FormulaCell", for: indexPath) as? FormulaCell {
                            if let songTitle = self.activeSongResult.object(forKey: "name") as? String {
                                cell.titleLabel.text = songTitle
                            }
                            var formula = ""
                            var ans = ""
                            if let tempStr = result.object(forKey: "label") as? String {
                                formula = tempStr
                            }
                            
                            cell.formulaLabel.text = formula
                            
                            if let isFinished = result.object(forKey: "isFinished") as? Bool {
                                if isFinished == true {
                                    
                                    if let tempStr = result.object(forKey: "value") as? String {
                                        ans = tempStr
                                    }
                                    
                                    var fomulaLabelValue = ""
                                    
                                    if let change_char = result.object(forKey: "change_char") as? String {
                                        if change_char.isEmpty == false {
                                            if formula.contains("X") {
                                                fomulaLabelValue = formula.replacingOccurrences(of: "X", with: ans)
                                            }else if formula.contains("x") {
                                                fomulaLabelValue = formula.replacingOccurrences(of: "x", with: ans)
                                            }else{
                                                fomulaLabelValue = "\(formula) \(ans)"
                                            }
                                            
                                        }else{
                                            fomulaLabelValue = "\(formula) \(ans)"
                                        }
                                    }
                                    
                                    cell.formulaLabel.text = fomulaLabelValue
//
                                    
                                }
                            }
                            
                            
                            return cell
                            
                        }
                        
                    }
                   
                }
            }
            
           
        }
        
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        if self.isSongMenuActivated == true {
            if let cell = collectionView.cellForItem(at: indexPath) as? SongListCell {
                if let status = cell.data.object(forKey: "status") as? String {
                    if status.caseInsensitiveCompare("Active") == .orderedSame {
                        if let songId = cell.data.object(forKey: "ID") as? String {
                            self.activeSongResult = NSDictionary(dictionary: cell.data)
                            self.getFormulaBySongId(songId: songId)
                        }
                        
                    }
                }
            }

        }
        
        
    }
    
    // MARK :- TO CHANGE WHILE CLICKING ON PAGE CONTROL
    @IBAction func prevButtonClick(){
        self.formulaCurrentIndex = self.formulaCurrentIndex - 1
        if self.formulaCurrentIndex <= 0 {
            self.formulaCurrentIndex = 0
        }
        var frame = self.collectionView.frame
        frame.origin.x = frame.size.width * CGFloat(self.formulaCurrentIndex)
        frame.origin.y = 0
        self.collectionView.scrollRectToVisible(frame, animated: true)
        self.pageController?.currentPage = self.formulaCurrentIndex
    }
    @IBAction func nextEquationCick(){
        self.formulaCurrentIndex = self.formulaCurrentIndex + 1
        var frame = self.collectionView.frame
        frame.origin.x = frame.size.width * CGFloat(self.formulaCurrentIndex)
        frame.origin.y = 0
        self.collectionView.scrollRectToVisible(frame, animated: true)
        self.pageController?.currentPage = self.formulaCurrentIndex
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        
        self.formulaCurrentIndex = Int(pageNumber)
        print("Page number = \(self.formulaCurrentIndex)")
        self.pageController?.currentPage = self.formulaCurrentIndex
        //        pageControl.currentPage = Int(pageNumber)
    }
   
    //MARK:- KeyBoard Delegate -
    func pianoKeyUp(_ keyNumber:Int) {
        print("pianoKeyUp")
        print(keyNumber)
        keyboardView.stopPulse(keyNum: keyNumber)
        
        self.VerifyFormulaByUserAction(keyNumber: keyNumber)
    }
    //Manage Keyboard
    func pianoKeyDown(_ keyNumber:Int) {
        print("pianoKeyDown")
        print(keyNumber)
        
        if self.soundIDs.count > keyNumber {
            DispatchQueue.main.async {
                AudioServicesPlaySystemSound(self.soundIDs[keyNumber])
                
            }
        }
        
    }
    func VerifyFormulaByUserAction(keyNumber : Int){
        let modifyKeyNumber = keyNumber + 1
        print(self.formulaCurrentIndex )
        if self.listOfFormulas.count > self.formulaCurrentIndex {
            if let result = self.listOfFormulas.object(at: self.formulaCurrentIndex) as? NSDictionary {
                if let answer = result.object(forKey: "key_value") as? String {
                    if let ke_value = Int(answer) {
                        if ke_value == modifyKeyNumber {
                            
                            self.popupsView?.isHidden = true
                            
                            var isLastSong : Bool = false
                            //checl song type
                            if let songType = result.object(forKey: "type") as? String {
                                if songType.caseInsensitiveCompare("Chord") == .orderedSame {
                                    if let imageUrl = result.object(forKey: "image") as? String {
                                        if let request = URL(string: imageUrl) {
                                            self.popupsImage?.af_setImage(withURL: request, placeholderImage: #imageLiteral(resourceName: "userDefault"), filter: nil, progress: nil, progressQueue: .main, imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: true, completion: { (response) in
                                                
                                                self.popupsView?.isHidden = false
                                                self.messageContainer?.isHidden = true
                                            })
                                        }
                                    }
                                }
                                else{
                                    if let songType = result.object(forKey: "type") as? String {
                                        if songType.caseInsensitiveCompare("last") == .orderedSame {
                                            self.isLastSongActivated = true
                                            self.nextButton?.isHidden = true
                                            self.prevButton?.isHidden = true
                                            isLastSong = true
                                            self.messageContainer?.isHidden = false
                                            //Update value in list
                                            
                                            self.messageLabel?.text = "You just finished one song! Click on to play along with the song"
                                            self.popupsImage?.image = #imageLiteral(resourceName: "MarcusFINAL-1")
                                            self.popupsView?.isHidden = false
                                        }
                                    }
                                    
                                }
                            }
                            
                            var finishedSongIndex : Int = 0
                            if isLastSong == false {
                                self.formulaCurrentIndex = self.formulaCurrentIndex + 1
                                finishedSongIndex = self.formulaCurrentIndex - 1
                                self.nextButton?.isHidden = false
                                self.prevButton?.isHidden = false
//                                self.popupsView?.isHidden = true
                                var frame = self.collectionView.frame
                                frame.origin.x = frame.size.width * CGFloat(self.formulaCurrentIndex)
                                self.collectionView.scrollRectToVisible(frame, animated: true)
                                
                                self.pageController?.currentPage = self.formulaCurrentIndex
                                
                            }else{
                                 finishedSongIndex = self.formulaCurrentIndex
                            }
                            
                            let mutableObject = NSMutableDictionary(dictionary: result)
                            mutableObject.setObject(true, forKey: "isFinished" as NSCopying)
                            print(mutableObject)
                            
                            if finishedSongIndex < self.listOfFormulas.count {
                                 self.listOfFormulas.replaceObject(at: finishedSongIndex, with: mutableObject)
                            }
                           
                            self.collectionView.reloadData()
                            
                            
                        }else{
                            self.messageLabel?.text = "Sorry!"
                            self.popupsImage?.image = #imageLiteral(resourceName: "MarcusFINAL-3")
                            self.popupsView?.isHidden = false
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- LastFormulaCell Delegate -
    func TestQuestionOption() {
        
        if let songId = self.activeSongResult.object(forKey: "ID") as? String {
            self.getTestQuestionBySongId(songId: songId)
        }
        
    }
    func TestQuestionAnswerPress(isRight : Bool){
        if isRight == true {
            self.messageLabel?.text = "Wow!"
            self.popupsImage?.image =  #imageLiteral(resourceName: "MarcusFINAL-1")
            self.popupsView?.isHidden = false
        }else{
            self.messageLabel?.text = "Sorry!"
            self.popupsImage?.image =  #imageLiteral(resourceName: "MarcusFINAL-3")
            self.popupsView?.isHidden = false
        }
    }
    func backToSongMenu() {
        self.isTestQuestionActivated = false
        self.isLastSongActivated = false
        self.isSongMenuActivated = true
        self.pageController?.isHidden = true
        self.collectionView.reloadData()
    }
    //MARK:- Make API Call -
    func GetSongList(){
      
        self.isSongMenuActivated = true
        
        var userId = ""
        if let idStr = getUserAuthentication().object(forKey: "userID") as? String {
            userId = idStr
        }
        
        let requestUrl = String(format: "%@?view=songs&userID=%@&catID=%@&subcateID=%@", arguments: [kRequestDomain,userId,self.catID, self.subcateID])
        
        
        print(requestUrl)
        appDelegate.DesapShowLoader()
        request(requestUrl, headers: nil).responseJSON { response in
            print(response)
            appDelegate.DesapHideLoader()
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
                if let jsonRes = json as? NSDictionary {
                    if let statusCode = jsonRes.object(forKey: "msgcode") as? String {
                        if statusCode == "0" {
                            if let songs = jsonRes.object(forKey: "song_list") as? NSArray {
                                self.listOfSongsInLive = NSArray(array: songs)
                                DispatchQueue.main.async {
                                    self.activeMenuName = "songs"
                                    self.CollectionViewUpdate()
                                }
                            }
//
                            
                        }else{
                            if let message = jsonRes.object(forKey: "message") as? String {
                                appDelegate.TNMShowMessage("", message: message)
//                                JYToast().isShow(message)
                            }
                        }
                    }
                }else{
                    JYToast().isShow(kErrorMessage)
                }
            }else{
                JYToast().isShow(kErrorMessage)
            }
        }
    }

    func getFormulaBySongId(songId : String) {
        
        self.listOfFormulas = NSMutableArray()
        
        var userId = ""
        if let idStr = getUserAuthentication().object(forKey: "userID") as? String {
            userId = idStr
        }
        
        let requestUrl = String(format: "%@?view=songs_eq&userID=%@&songsID=%@", arguments: [kRequestDomain,userId,songId])
        
        print(requestUrl)
        appDelegate.DesapShowLoader()
        request(requestUrl, headers: nil).responseJSON { response in
            print(response)
            appDelegate.DesapHideLoader()
            
            
            if let json = response.result.value {
                
                if let jsonRes = json as? NSDictionary {
                    if let statusCode = jsonRes.object(forKey: "msgcode") as? String {
                        if statusCode == "0" {
                            if let songs = jsonRes.object(forKey: "song_eq") as? NSArray {
                                
                                for tempFormula in songs {
                                    if let formula = tempFormula as? NSDictionary {
                                        let mutableFormula = NSMutableDictionary(dictionary: formula)
                                        mutableFormula.setObject(false, forKey: "isFinished" as NSCopying)
                                        self.listOfFormulas.add(mutableFormula)
                                    }
                                }
                                self.pageController?.numberOfPages = self.listOfFormulas.count
//                                self.listOfFormulas = NSArray(array: songs)
                                self.isSongMenuActivated = false
                                self.pageController?.isHidden = false
                                self.nextButton?.isHidden = false
                                self.prevButton?.isHidden = false
                                self.formulaCurrentIndex = 0
                                self.collectionView.reloadData()
                                
                                var frame = self.collectionView.frame
                                frame.origin.x = frame.size.width * CGFloat(self.formulaCurrentIndex)
                                self.collectionView.scrollRectToVisible(frame, animated: false)
                                self.pageController?.currentPage = self.formulaCurrentIndex
                                self.getSongCompleteImageandMp3(songId: songId)
                            }
                            //
                            
                        }else{
                            if let message = jsonRes.object(forKey: "message") as? String {
                                appDelegate.TNMShowMessage("", message: message)
                            }
                        }
                    }
                }else{
                    JYToast().isShow(kErrorMessage)
                }
            }else{
                JYToast().isShow(kErrorMessage)
            }
        }
    }
    func getTestQuestionBySongId(songId : String) {
        
        self.listOfTestQuestions = NSMutableArray()
        
        var userId = ""
        if let idStr = getUserAuthentication().object(forKey: "userID") as? String {
            userId = idStr
        }
        
        let requestUrl = String(format: "%@?view=song_question&userID=%@&songsID=%@", arguments: [kRequestDomain,userId,songId])
        
        print(requestUrl)
        appDelegate.DesapShowLoader()
        request(requestUrl, headers: nil).responseJSON { response in
            print(response)
            appDelegate.DesapHideLoader()
            
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
                if let jsonRes = json as? NSDictionary {
                    if let statusCode = jsonRes.object(forKey: "msgcode") as? String {
                        if statusCode == "0" {
                            if let songs = jsonRes.object(forKey: "question_list") as? NSArray {
                                
                                for tempFormula in songs {
                                    if let formula = tempFormula as? NSDictionary {
                                        let mutableFormula = NSMutableDictionary(dictionary: formula)
                                        mutableFormula.setObject(false, forKey: "isFinished" as NSCopying)
                                        self.listOfTestQuestions.add(mutableFormula)
                                    }
                                }
                                if self.listOfFormulas.count > 1 {
                                    self.pageController?.isHidden = false
                                }else{
                                    self.pageController?.isHidden = true
                                }
                                self.pageController?.numberOfPages = self.listOfFormulas.count
                               
                                self.isTestQuestionActivated = true
                                self.formulaCurrentIndex = 0
                                self.isLastSongActivated = false
                                self.isSongMenuActivated = false
                                self.pageController?.isHidden = false
                                self.collectionView.reloadData()
                                
                                
                            }
                            //
                            
                        }else{
                            if let message = jsonRes.object(forKey: "message") as? String {
                                appDelegate.TNMShowMessage("", message: message)
                            }
                        }
                    }
                }else{
                    JYToast().isShow(kErrorMessage)
                }
            }else{
                JYToast().isShow(kErrorMessage)
            }
        }
    }
    
    
    func getSongCompleteImageandMp3(songId : String) {
        
        self.songCompaleteResult = NSDictionary()
        
        var userId = ""
        if let idStr = getUserAuthentication().object(forKey: "userID") as? String {
            userId = idStr
        }
        
        let requestUrl = String(format: "%@?view=songs_complete&userID=%@&songsID=%@", arguments: [kRequestDomain,userId,songId])
        
        print(requestUrl)
        
        request(requestUrl, headers: nil).responseJSON { response in
            print(response)
        
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
                if let jsonRes = json as? NSDictionary {
                    if let statusCode = jsonRes.object(forKey: "msgcode") as? String {
                        if statusCode == "0" {
                            self.songCompaleteResult = NSDictionary(dictionary: jsonRes)
                            
                        }else{
                            if let message = jsonRes.object(forKey: "message") as? String {
                                appDelegate.TNMShowMessage("", message: message)
                            }
                        }
                    }
                }else{
                    JYToast().isShow(kErrorMessage)
                }
            }else{
                JYToast().isShow(kErrorMessage)
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.popupsView?.isHidden = true
        
        if self.isLastSongActivated == true {
            self.listOfFormulas.removeAllObjects()
            self.listOfFormulas.add(self.songCompaleteResult)
            self.collectionView.reloadData()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


