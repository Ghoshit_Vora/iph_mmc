//
//  SongListCell.swift
//  Piano
//
//  Created by Desap Team on 08/12/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit

class SubjectCell: UICollectionViewCell {

    @IBOutlet var playButtonTitle : UILabel?
    
    @IBOutlet var songImage : UIImageView!
    
    var data = NSDictionary()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
