//
//  SubjectViewController.swift
//  Piano
//
//  Created by Desap Team on 09/02/2018.
//  Copyright © 2018 Piano. All rights reserved.
//

import UIKit

class SubjectViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionview : UICollectionView?
    var listOfSubjects = NSArray()
    var listOfSubCategory = NSArray()
    var isActivteMainCategory : Bool = true
    var mainCategoryId : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        self.GetCategoryList()
    }

    func CollectionViewUpdate(){
        
        let nibName = UINib(nibName: "SubjectCell", bundle:nil)
        self.collectionview?.register(nibName, forCellWithReuseIdentifier: "SubjectCell")
        
        
        self.collectionview?.dataSource = self
        self.collectionview?.delegate = self
        self.collectionview?.reloadData()
    }
    
    //MARK: - UICollectionView Delegate -
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.size.width / 3, height: collectionView.bounds.size.width / 3);
        
        /*
        if self.isActivteMainCategory == true {
            let count = CGFloat(self.listOfSubjects.count)
            if count > 2 {
                return CGSize(width: collectionView.bounds.size.width / 3, height: collectionView.bounds.size.width / 3);
            }else{
                return CGSize(width: collectionView.bounds.size.width / count, height: collectionView.bounds.size.width / count);
            }
        }else{
            let count = CGFloat(self.listOfSubCategory.count)
            if count > 2 {
                return CGSize(width: collectionView.bounds.size.width / 3, height: collectionView.bounds.size.width / 3);
            }else{
                return CGSize(width: collectionView.bounds.size.width / count, height: collectionView.bounds.size.width / count);
            }
        }
        return CGSize(width: 0, height: 0);
        */
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.isActivteMainCategory == true {
            return self.listOfSubjects.count
        }else{
            return self.listOfSubCategory.count
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubjectCell", for: indexPath) as? SubjectCell {
            
            var result = NSDictionary()
            if self.isActivteMainCategory == true {
                if self.listOfSubjects.count > indexPath.row {
                    if let resultTemp = self.listOfSubjects.object(at: indexPath.row) as? NSDictionary {
                        result = NSDictionary(dictionary: resultTemp)
                    }
                }
            }else{
                if self.listOfSubCategory.count > indexPath.row {
                    if let resultTemp = self.listOfSubCategory.object(at: indexPath.row) as? NSDictionary {
                        result = NSDictionary(dictionary: resultTemp)
                    }
                }
            }
            
            cell.data = result
            if let imageUrl = result.object(forKey: "image") as? String {
                if let request = URL(string: imageUrl) {
                    cell.songImage.af_setImage(withURL: request, placeholderImage: #imageLiteral(resourceName: "userDefault"), filter: nil, progress: nil, progressQueue: .main, imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: true, completion: { (response) in
                        
                        if let image = response.result.value {
                            if image.size.width + 5 < image.size.height {
                                cell.songImage.contentMode = .scaleAspectFit
                            }
                            
                        }
                        
                    })
                }
                
            }
            if let name = result.object(forKey: "name") as? String {
                cell.playButtonTitle?.text = "  \(name)  "
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        if self.isActivteMainCategory == true {
            if let cell = collectionView.cellForItem(at: indexPath) as? SubjectCell {
                if let sub_cats = cell.data.object(forKey: "sub_cats") as? String {
                    if sub_cats.caseInsensitiveCompare("yes") == .orderedSame {
                        if let songId = cell.data.object(forKey: "catID") as? String {
                            self.isActivteMainCategory = false
                            self.mainCategoryId = songId
                            self.GetSubCategoryById(catId: songId)
                        }

                    }else{
                        if let sub_cats = cell.data.object(forKey: "catID") as? String {
                            let controller = PlaySongsViewController(nibName: "PlaySongsViewController", bundle: nil)
                            controller.catID = sub_cats
                            controller.subcateID = ""
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                }
            }

        }else{
            if let cell = collectionView.cellForItem(at: indexPath) as? SubjectCell {
                if let sub_cats = cell.data.object(forKey: "subcatID") as? String {
                    let controller = PlaySongsViewController(nibName: "PlaySongsViewController", bundle: nil)
                    controller.catID = self.mainCategoryId
                    controller.subcateID = sub_cats
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
        
        
    }
    
    //MARK:- Make API Call -
    func GetCategoryList(){
        
        var userId = ""
        if let idStr = getUserAuthentication().object(forKey: "userID") as? String {
            userId = idStr
        }
        
        let requestUrl = String(format: "%@?view=category&userID=%@", arguments: [kRequestDomain,userId])
        
        
        print(requestUrl)
        appDelegate.DesapShowLoader()
        request(requestUrl, headers: nil).responseJSON { response in
            print(response)
            appDelegate.DesapHideLoader()
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
                if let jsonRes = json as? NSDictionary {
                    if let statusCode = jsonRes.object(forKey: "msgcode") as? String {
                        if statusCode == "0" {
                            if let songs = jsonRes.object(forKey: "category_list") as? NSArray {
                                self.listOfSubjects = NSArray(array: songs)
                                DispatchQueue.main.async {
                                   
                                    self.CollectionViewUpdate()
                                }
                            }
                            //
                            
                        }else{
                            if let message = jsonRes.object(forKey: "message") as? String {
//                                JYToast().isShow(message)
                                 appDelegate.TNMShowMessage("", message: message)
                            }
                        }
                    }
                }else{
                    JYToast().isShow(kErrorMessage)
                }
            }else{
                JYToast().isShow(kErrorMessage)
            }
        }
    }
    
    //MARK:- Make API Call -
    func GetSubCategoryById(catId : String){
        
        
        var userId = ""
        if let idStr = getUserAuthentication().object(forKey: "userID") as? String {
            userId = idStr
        }
        
        let requestUrl = String(format: "%@?view=sub_category&userID=%@&catID=%@", arguments: [kRequestDomain,userId,catId])
        
        print(requestUrl)
        appDelegate.DesapShowLoader()
        request(requestUrl, headers: nil).responseJSON { response in
            print(response)
            appDelegate.DesapHideLoader()
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
                if let jsonRes = json as? NSDictionary {
                    if let statusCode = jsonRes.object(forKey: "msgcode") as? String {
                        if statusCode == "0" {
                            if let songs = jsonRes.object(forKey: "sub_category_list") as? NSArray {
                                self.listOfSubCategory = NSArray(array: songs)
                                DispatchQueue.main.async {
                                    
                                    self.CollectionViewUpdate()
                                }
                            }
                            //
                            
                        }else{
                            if let message = jsonRes.object(forKey: "message") as? String {
                                appDelegate.TNMShowMessage("", message: message)
//                                JYToast().isShow(message)
                            }
                        }
                    }
                }else{
                    JYToast().isShow(kErrorMessage)
                }
            }else{
                JYToast().isShow(kErrorMessage)
            }
        }
    }
    
    @IBAction func openMainMenu(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
