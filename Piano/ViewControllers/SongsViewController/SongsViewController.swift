//
//  SongsViewController.swift
//  Piano
//
//  Created by Desap Team on 07/12/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox


class SongsViewController: UIViewController,GLNPianoViewDelegate {
   
    @IBOutlet var lessonImageview1 : UIImageView?
    @IBOutlet var lessonImageview2 : UIImageView?
    
    var soundIDs = [SystemSoundID]()
    @IBOutlet weak var keyboardView: GLNPianoView! {
        didSet{
            keyboardView.delegate = self
        }
    }
    
    var activeMenuName = "lessons"
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        
        DispatchQueue.main.async {
            self.LoadPianoSound()
        }
    }
    func LoadPianoSound(){
        
        if let path = Bundle.main.path(forResource: "keyboardLayout", ofType: "plist") {
            
            //If your plist contain root as Array
            if let array = NSArray(contentsOfFile: path) {
                for (_, result) in array.enumerated() {
                    
                    if let pathStr = result as? String {
                        let sndurl = Bundle.main.url(forResource:pathStr, withExtension: "wav")!
                        var SoundID : SystemSoundID = 0
                        AudioServicesCreateSystemSoundID(sndurl as CFURL, &SoundID)
                        /*
                         // watch _this_ little move
                         AudioServicesAddSystemSoundCompletion(SoundID, nil, nil, {
                         sound, context in
                         print("finished!")
                         AudioServicesRemoveSystemSoundCompletion(sound)
                         AudioServicesDisposeSystemSoundID(sound)
                         }, nil)
                         */
                        self.soundIDs.append(SoundID)
                    }
                    
                }
            }
            
        }
        
        
        
    }
    
    
    @IBAction func optionClick() {
        if self.activeMenuName == "lessons" {
            self.activeMenuName = "songs"
            self.lessonImageview1?.image = #imageLiteral(resourceName: "songs1")
            self.lessonImageview2?.image = #imageLiteral(resourceName: "songs2")
        }else if self.activeMenuName == "songs" {
            self.activeMenuName = "play"
            let controller = PlaySongsViewController(nibName: "PlaySongsViewController", bundle: nil)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func openMainMenu(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    
    //MARK:- KeyBoard Delegate -
    func pianoKeyUp(_ keyNumber:Int) {
        print("pianoKeyUp")
        print(keyNumber)
        //        audioEngine.sampler.stopNote(UInt8(octave + keyNumber), onChannel: 0)
        
        keyboardView.stopPulse(keyNum: keyNumber)
        //        self.pulseNextStep(keyNumber: keyNumber)
    }
    //Manage Keyboard
    func pianoKeyDown(_ keyNumber:Int) {
        print("pianoKeyDown")
        print(keyNumber)
        //        audioEngine.sampler.startNote(UInt8(octave + keyNumber), withVelocity: 64, onChannel: 0)
        //        audioEngine.sampler.startNote(UInt8(octave + keyNumber), withVelocity: 64, onChannel: 0)
        
        //        self.stopAllSounds()
        
        
        if self.soundIDs.count > keyNumber {
            DispatchQueue.main.async {
                AudioServicesPlaySystemSound(self.soundIDs[keyNumber])
            }
            
        }
       
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



