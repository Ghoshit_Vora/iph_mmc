//
//  LoginViewController.swift
//  Piano
//
//  Created by Desap Team on 13/10/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController,BSKeyboardControlsDelegate, UITextFieldDelegate {

    @IBOutlet var usernameField : TNMTextField!
    
    
    @IBOutlet var headerView : UIView!
    @IBOutlet var tableview : UITableView!
    
    var keyboard: BSKeyboardControls!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        self.usernameField.delegate = self
        
        keyboard = BSKeyboardControls(fields: [usernameField])
        
        keyboard.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    @IBAction func backClick(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func SignUpClick(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
        DispatchQueue.main.async {
            
            if UIDevice.current.userInterfaceIdiom == .phone {
                var frame = UIScreen.main.bounds
                frame.size.height = 700
                self.headerView.frame = frame
                self.tableview.tableHeaderView = self.headerView
            }else{
                let frame = UIScreen.main.bounds
                self.headerView.frame = frame
                self.tableview.tableHeaderView = self.headerView
            }
            
        }
        
    }
    @IBAction func loginClick(){
        self.ForgotPassword()
    }

    //MARK:- SignUpRequest -
    func ForgotPassword() {
        
        var username = ""
        if let text = usernameField.text {
            username = text
            if username.isEmpty == true {
                appDelegate.TNMShowMessage("", message: "Please enter username.")
                return
            }
        }
        
        
        let requestUrl = String(format: "%@?view=forget_pass&username=%@", arguments: [kRequestDomain,username])
        
        print(requestUrl)
        appDelegate.DesapShowLoader()
        request(requestUrl, headers: nil).responseJSON { response in
            print(response)
            appDelegate.DesapHideLoader()
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
                if let jsonRes = json as? NSDictionary {
                    if let statusCode = jsonRes.object(forKey: "msgcode") as? String {
                        if statusCode == "0" {
                            
                            if let message = jsonRes.object(forKey: "message") as? String {
                                JYToast().isShow(message)
                            }
                            
                            let controller = LoginViewController(nibName: "LoginViewController", bundle: nil)
                            self.navigationController?.pushViewController(controller, animated: true)
                            
                        }else{
                            if let message = jsonRes.object(forKey: "message") as? String {
                                JYToast().isShow(message)
                            }
                        }
                    }
                }else{
                    JYToast().isShow(kErrorMessage)
                }
            }else{
                JYToast().isShow(kErrorMessage)
            }
        }
        
        
    }
    
    @IBAction func forgotPassword(){
        let alertController = UIAlertController(title: "Forgot your password?", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addTextField { (textField : UITextField) -> Void in
            textField.placeholder = "Enter email"
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        let okAction = UIAlertAction(title: "Send Email", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Keyboard Event -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboard.activeField = textField
    }
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, inDirection direction: BSKeyboardControlsDirection) {
        self.tableview.scrollRectToVisible(field.frame, animated: true)
    }
    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        self.view.endEditing(true)
    }
    @objc func onKeyboardHide(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let duration = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsets.zero;
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
        })
    }
    @objc func onKeyboardShow(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let kbMain  = (userInfo.object(forKey: UIKeyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue
        let kbSize = kbMain?.size
        let duration  = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double;
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsetsMake(0, 0, (kbSize?.height)!, 0)
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
            if (self.keyboard.activeField) != nil
            {
                self.tableview.scrollRectToVisible(self.keyboard.activeField!.frame, animated: true)
            }
        })
    }
    
    
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
