//
//  TutorialViewController.swift
//  Piano
//
//  Created by Desap Team on 06/03/2018.
//  Copyright © 2018 Piano. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox
import AVKit

class TutorialViewController: UIViewController,GLNPianoViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionView : UICollectionView!
    var listOfTutorialsInLive = NSArray()
    
    var soundIDs = [SystemSoundID]()
    @IBOutlet weak var keyboardView: GLNPianoView! {
        didSet{
            keyboardView.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.main.async {
            self.LoadPianoSound()
        }
        
        self.GetTutorialsList()
    }
    func LoadPianoSound(){
        
        if let path = Bundle.main.path(forResource: "keyboardLayout", ofType: "plist") {
            
            //If your plist contain root as Array
            if let array = NSArray(contentsOfFile: path) {
                for (_, result) in array.enumerated() {
                    
                    if let pathStr = result as? String {
                        let sndurl = Bundle.main.url(forResource:pathStr, withExtension: "wav")!
                        var SoundID : SystemSoundID = 0
                        AudioServicesCreateSystemSoundID(sndurl as CFURL, &SoundID)
                        /*
                         // watch _this_ little move
                         AudioServicesAddSystemSoundCompletion(SoundID, nil, nil, {
                         sound, context in
                         print("finished!")
                         AudioServicesRemoveSystemSoundCompletion(sound)
                         AudioServicesDisposeSystemSoundID(sound)
                         }, nil)
                         */
                        self.soundIDs.append(SoundID)
                    }
                    
                }
            }
            
        }
        
        
        
    }
    
    //MARK: - UICollectionView Delegate -
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.size.width / 3, height: collectionView.bounds.size.width / 3);
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listOfTutorialsInLive.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SongListCell", for: indexPath) as? SongListCell {
            if self.listOfTutorialsInLive.count > indexPath.row {
                if let result = self.listOfTutorialsInLive.object(at: indexPath.row) as? NSDictionary {
                    cell.data = result
                    
                    if let status = result.object(forKey: "status") as? String {
                        if status.caseInsensitiveCompare("Active") == .orderedSame {
                            cell.songBackground?.image = nil
                            
                            cell.songBackground?.backgroundColor = NSTheme().GetActiveSongBG()
                            cell.playButtonTitle?.text = "Play"
                            cell.songName.textColor = UIColor.white
                            cell.playButtonTitle?.textColor = UIColor.blue
                        }else{
                            cell.songBackground?.image = #imageLiteral(resourceName: "sound2")
                            cell.playButtonTitle?.text = "Locked"
                            cell.songName.textColor = UIColor.darkGray
                            cell.playButtonTitle?.textColor = UIColor.darkGray
                        }
                    }
                    
                    if let imageUrl = result.object(forKey: "image") as? String {
                        if let request = URL(string: imageUrl) {
                            cell.songImage.af_setImage(withURL: request, placeholderImage: #imageLiteral(resourceName: "userDefault"), filter: nil, progress: nil, progressQueue: .main, imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: true, completion: { (response) in
                                
                                if let image = response.result.value {
                                    if image.size.width + 5 < image.size.height {
                                        cell.songImage.contentMode = .scaleAspectFit
                                    }
                                    
                                }
                                
                            })
                        }
                        
                    }
                    if let name = result.object(forKey: "name") as? String {
                        cell.songName.text = name
                    }
                    
                }
            }
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        if let cell = collectionView.cellForItem(at: indexPath) as? SongListCell {
            if let status = cell.data.object(forKey: "status") as? String {
                if status.caseInsensitiveCompare("Active") == .orderedSame {
                    if let videoUrl = cell.data.object(forKey: "url") as? String {
                        print("Video URL:\(videoUrl)")
                        self.PlayHelpVideo(url: videoUrl)
                    }
                    
                }
            }
        }
        
        
    }
    func PlayHelpVideo(url : String){
        // Movie player
        if let videoUrl = URL(string: url) {
            let player = AVPlayer(url: videoUrl)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
        
    }
    //MARK:- Make API Call -
    func GetTutorialsList(){
        
        
        var userId = ""
        if let idStr = getUserAuthentication().object(forKey: "userID") as? String {
            userId = idStr
        }
        
        let requestUrl = String(format: "%@?view=tutorials&userID=%@&songsID=", arguments: [kRequestDomain,userId])
        
        
        print(requestUrl)
        appDelegate.DesapShowLoader()
        request(requestUrl, headers: nil).responseJSON { response in
            print(response)
            appDelegate.DesapHideLoader()
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
                if let jsonRes = json as? NSDictionary {
                    if let statusCode = jsonRes.object(forKey: "msgcode") as? String {
                        if statusCode == "0" {
                            if let songs = jsonRes.object(forKey: "tutorial_list") as? NSArray {
                                self.listOfTutorialsInLive = NSArray(array: songs)
                                DispatchQueue.main.async {
                                    
                                    self.CollectionViewUpdate()
                                }
                            }
                            //
                            
                        }else{
                            if let message = jsonRes.object(forKey: "message") as? String {
                                appDelegate.TNMShowMessage("", message: message)
                                //                                JYToast().isShow(message)
                            }
                        }
                    }
                }else{
                    JYToast().isShow(kErrorMessage)
                }
            }else{
                JYToast().isShow(kErrorMessage)
            }
        }
    }
    func CollectionViewUpdate(){
        
        let nibName = UINib(nibName: "SongListCell", bundle:nil)
        self.collectionView.register(nibName, forCellWithReuseIdentifier: "SongListCell")
        
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.reloadData()
    }
    //MARK:- KeyBoard Delegate -
    func pianoKeyUp(_ keyNumber:Int) {
        print("pianoKeyUp")
        print(keyNumber)
        //        audioEngine.sampler.stopNote(UInt8(octave + keyNumber), onChannel: 0)
        
        keyboardView.stopPulse(keyNum: keyNumber)
        //        self.pulseNextStep(keyNumber: keyNumber)
    }
    //Manage Keyboard
    func pianoKeyDown(_ keyNumber:Int) {
        print("pianoKeyDown")
        print(keyNumber)
        //        audioEngine.sampler.startNote(UInt8(octave + keyNumber), withVelocity: 64, onChannel: 0)
        //        audioEngine.sampler.startNote(UInt8(octave + keyNumber), withVelocity: 64, onChannel: 0)
        
        //        self.stopAllSounds()
        
        
        if self.soundIDs.count > keyNumber {
            DispatchQueue.main.async {
                AudioServicesPlaySystemSound(self.soundIDs[keyNumber])
            }
            
        }
        
        
    }
    @IBAction func openMainMenu(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
