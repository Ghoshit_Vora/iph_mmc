//
//  LoginViewController.swift
//  Piano
//
//  Created by Desap Team on 13/10/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,BSKeyboardControlsDelegate, UITextFieldDelegate {

    @IBOutlet var headerView : UIView!
    @IBOutlet var tableview : UITableView!
    
    @IBOutlet var usernameField : TNMTextField!
    @IBOutlet var passwordField : TNMTextField!
    @IBOutlet var emailField : TNMTextField!
    @IBOutlet var phoneField : TNMTextField!
    
    var keyboard: BSKeyboardControls!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.usernameField.delegate = self
        self.passwordField.delegate = self
        self.emailField.delegate = self
        self.phoneField.delegate = self
        keyboard = BSKeyboardControls(fields: [usernameField,passwordField,emailField,phoneField])
        
        keyboard.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        DispatchQueue.main.async {
            
            if UIDevice.current.userInterfaceIdiom == .phone {
                var frame = UIScreen.main.bounds
                frame.size.height = 700
                self.headerView.frame = frame
                self.tableview.tableHeaderView = self.headerView
            }else{
                let frame = UIScreen.main.bounds
                self.headerView.frame = frame
                self.tableview.tableHeaderView = self.headerView
            }
            
        }
       
    }
    @IBAction func loginClick(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func signUpClick(){
        
        self.SignUpRequest()
        
//        let alert = UIAlertController(title: "Make Music Count", message: "Register successfully.", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
//            let controller = DashboardViewController(nibName: "DashboardViewController", bundle: nil)
//            self.navigationController?.pushViewController(controller, animated: true)
//        }))
//        
//        self.present(alert, animated: true, completion: nil)
        
    }

    //MARK:- SignUpRequest -
    func SignUpRequest() {
        
        var userName = ""
        if let text = usernameField.text {
            userName = text
            if userName.isEmpty == true {
                appDelegate.TNMShowMessage("", message: "Please enter username.")
                return
            }
        }
        var password = ""
        if let text = passwordField.text {
            password = text
            if password.isEmpty == true {
                appDelegate.TNMShowMessage("", message: "Please enter password.")
                return
            }
        }
        
        var email = ""
        if let text = emailField.text {
            email = text
            if email.isEmpty == true {
                appDelegate.TNMShowMessage("", message: "Please enter email.")
                return
            }
        }
        
        var phone = ""
        if let text = phoneField.text {
            phone = text
            if phone.isEmpty == true {
                appDelegate.TNMShowMessage("", message: "Please enter phone number.")
                return
            }
        }
        
        let requestUrl = String(format: "%@?view=register&username=%@&useremail=%@&userpass=%@&userphone=%@", arguments: [kRequestDomain,userName,email,password,phone])
        print(requestUrl)
        appDelegate.DesapShowLoader()
        request(requestUrl, headers: nil).responseJSON { response in
            print(response)
            appDelegate.DesapHideLoader()
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
                if let jsonRes = json as? NSDictionary {
                    if let statusCode = jsonRes.object(forKey: "msgcode") as? String {
                        if statusCode == "0" {
                            
                            if let message = jsonRes.object(forKey: "message") as? String {
                                JYToast().isShow(message)
                            }
                            if let userDetails = jsonRes.object(forKey: "detail") as? NSArray {
                                if userDetails.count > 0 {
                                    if let userData = userDetails.object(at: 0) as? NSDictionary {
                                        setUserAuthentication(userData)
                                    }
                                }
                            }
                            
                            let controller = OTPViewController(nibName: "OTPViewController", bundle: nil)
                            if let otp = jsonRes.object(forKey: "otp") as? String {
                                controller.OneTimePassword = otp
                            }
                            self.navigationController?.pushViewController(controller, animated: true)
                            
                        }else{
                            if let message = jsonRes.object(forKey: "message") as? String {
                                appDelegate.TNMShowMessage("", message: message)
                            }
                        }
                    }
                }else{
                    JYToast().isShow(kErrorMessage)
                }
            }else{
                JYToast().isShow(kErrorMessage)
            }
        }
        
        
    }
    
    // MARK: - Keyboard Event -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboard.activeField = textField
    }
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, inDirection direction: BSKeyboardControlsDirection) {
        self.tableview.scrollRectToVisible(field.frame, animated: true)
    }
    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        self.view.endEditing(true)
    }
    @objc func onKeyboardHide(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let duration = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsets.zero;
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
        })
    }
    @objc func onKeyboardShow(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let kbMain  = (userInfo.object(forKey: UIKeyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue
        let kbSize = kbMain?.size
        let duration  = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double;
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsetsMake(0, 0, (kbSize?.height)!, 0)
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
            if (self.keyboard.activeField) != nil
            {
                self.tableview.scrollRectToVisible(self.keyboard.activeField!.frame, animated: true)
            }
        })
    }
    
    
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
