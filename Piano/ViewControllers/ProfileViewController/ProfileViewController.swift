//
//  ProfileViewController.swift
//  Piano
//
//  Created by Desap Team on 28/10/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,BSKeyboardControlsDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var nameTextField : TNMTextField?
    @IBOutlet var emailTextField : TNMTextField?
    @IBOutlet var phoneTextField : TNMTextField?
    @IBOutlet var passwordTextField : TNMTextField?
    @IBOutlet var userimageview : UIImageView?
    var updatedImage : UIImage?
    
    var keyboard: BSKeyboardControls!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.navigationItem.title = "Profile"
        // Do any additional setup after loading the view.
        keyboard = BSKeyboardControls(fields: [nameTextField!,emailTextField!,phoneTextField!,passwordTextField!])
        
        keyboard.delegate = self
        
        
        self.userimageview?.layer.cornerRadius = (self.userimageview?.bounds.size.width)! / 2
        self.userimageview?.clipsToBounds = true
        
     
        self.updateUserIU()
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(OpenImagePickerView))
        self.userimageview?.isUserInteractionEnabled = true
        self.userimageview?.addGestureRecognizer(tapGestureRecognizer)
        
    }

    func updateUserIU(){
        if let username = getUserAuthentication().object(forKey: "name") as? String {
            self.nameTextField?.text = username
        }
        if let username = getUserAuthentication().object(forKey: "email") as? String {
            self.emailTextField?.text = username
        }
        if let username = getUserAuthentication().object(forKey: "phone") as? String {
            self.phoneTextField?.text = username
        }
        if let imageUrl = getUserAuthentication().object(forKey: "image") as? String {
            if let request = URL(string: imageUrl) {
                self.userimageview?.af_setImage(withURL: request, placeholderImage: #imageLiteral(resourceName: "userDefault"), filter: nil, progress: nil, progressQueue: .main, imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: true, completion: { (response) in
                    
                    
                })
            }
            
        }
    }
    //MARK: - OpenImagePickerView
    
    @objc func OpenImagePickerView(){
        
        let refreshAlert = UIAlertController(title: "Select Profile Photo", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        refreshAlert.addAction(UIAlertAction(title: "Take photo", style: .default, handler: { (action: UIAlertAction!) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Select from camera roll", style: .default, handler: { (action: UIAlertAction!) in
            
            let imagePicker1 = UIImagePickerController()
            imagePicker1.delegate = self
            imagePicker1.allowsEditing = false
            imagePicker1.sourceType = .photoLibrary
            imagePicker1.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(imagePicker1, animated: true, completion: nil)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            
        }))
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad )
        {
            if let currentPopoverpresentioncontroller = refreshAlert.popoverPresentationController{
                currentPopoverpresentioncontroller.sourceView = self.userimageview
                currentPopoverpresentioncontroller.sourceRect = (self.userimageview?.bounds)!;
                currentPopoverpresentioncontroller.permittedArrowDirections = UIPopoverArrowDirection.up;
                self.present(refreshAlert, animated: true, completion: nil)
            }
        }else{
            self.present(refreshAlert, animated: true, completion: nil)
        }
        
        
//        present(refreshAlert, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let mediaType = info[UIImagePickerControllerMediaType] as? String {
            
            print(mediaType)
            
            if mediaType  == "public.image" {
                print("Image Selected")
                
                if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    self.userimageview?.image = chosenImage
                    self.updatedImage = chosenImage
                }
                
            }
            else if mediaType == "public.movie" {
                print("Video Selected")
                appDelegate.TNMShowMessage("", message: "Please select image only")
            }
        }
        
        dismiss(animated:true, completion: nil) //5
    }
    
    @IBAction func updateClick(){
        self.updateUserProfile()
    }
    @IBAction func backClick(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func openMainMenu(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK:- Update profile -
    func updateUserProfile(){
        var image = self.userimageview?.image
        if image == nil {
            image = #imageLiteral(resourceName: "userDefault")
        }
        let imgData = UIImageJPEGRepresentation(image!, 0.2)!
        
        var userId = ""
        if let idStr = getUserAuthentication().object(forKey: "userID") as? String {
            userId = idStr
        }
        
        let requestUrl = String(format: "%@?view=change_info", arguments: [kRequestDomain])
        
        var username = ""
        if let tempStr = self.nameTextField?.text {
            username = tempStr
        }
        var useremail = ""
        if let tempStr = self.emailTextField?.text {
            useremail = tempStr
        }
        
        var userphone = ""
        if let tempStr = self.phoneTextField?.text {
            userphone = tempStr
        }
        var userpass = ""
        if let tempStr = self.passwordTextField?.text {
            userpass = tempStr
        }
        
        let parameters = ["username": username,"useremail":useremail,"userphone":userphone,"userpass":userpass,"userID":userId]
        print(parameters)
//        username, useremail, userphone, userpass, userphoto, userID   ------- All data in Post
        upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "userphoto",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },
                         to:requestUrl)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                print(result)
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    if let json = response.result.value {
                        print("JSON: \(json)") // serialized json response
                        if let jsonRes = json as? NSDictionary {
                            if let statusCode = jsonRes.object(forKey: "msgcode") as? String {
                                if statusCode == "0" {
                                    
                                    
                                    if let message = jsonRes.object(forKey: "message") as? String {
                                        appDelegate.TNMShowMessage("", message: message)
                                    }
                                    if let userDetails = jsonRes.object(forKey: "detail") as? NSArray {
                                        if userDetails.count > 0 {
                                            if let userData = userDetails.object(at: 0) as? NSDictionary {
                                                setUserAuthentication(userData)
                                            }
                                        }
                                    }
                                    
                                    //update ui
                                    self.updateUserIU()
                                    
                                }else{
                                    if let message = jsonRes.object(forKey: "message") as? String {
                                        appDelegate.TNMShowMessage("", message: message)
                                    }
                                }
                            }
                        }else{
                            JYToast().isShow(kErrorMessage)
                        }
                    }else{
                        JYToast().isShow(kErrorMessage)
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    // MARK: - Keyboard Event -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboard.activeField = textField
    }
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, inDirection direction: BSKeyboardControlsDirection) {
//        self.tableview.scrollRectToVisible(field.frame, animated: true)
    }
    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        self.view.endEditing(true)
    }
    @objc func onKeyboardHide(_ notification: Notification)
    {
//        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
//        let duration = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double
//        UIView.animate(withDuration: duration, animations: { () -> Void in
//            let edgeInsets  = UIEdgeInsets.zero;
//            self.tableview.contentInset = edgeInsets
//            self.tableview.scrollIndicatorInsets = edgeInsets
//        })
    }
    @objc func onKeyboardShow(_ notification: Notification)
    {
//        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
//        let kbMain  = (userInfo.object(forKey: UIKeyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue
//        let kbSize = kbMain?.size
//        let duration  = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double;
//        UIView.animate(withDuration: duration, animations: { () -> Void in
//            let edgeInsets  = UIEdgeInsetsMake(0, 0, (kbSize?.height)!, 0)
//            self.tableview.contentInset = edgeInsets
//            self.tableview.scrollIndicatorInsets = edgeInsets
//            if (self.keyboard.activeField) != nil
//            {
//                self.tableview.scrollRectToVisible(self.keyboard.activeField!.frame, animated: true)
//            }
//        })
    }
    
    
    
    //MARK: - ImagePickerView
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newHeight, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newHeight, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
