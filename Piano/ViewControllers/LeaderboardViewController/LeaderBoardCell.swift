//
//  LeaderBoardCell.swift
//  Piano
//
//  Created by Desap Team on 06/03/2018.
//  Copyright © 2018 Piano. All rights reserved.
//

import UIKit

class LeaderBoardCell: UITableViewCell {

    @IBOutlet var usernameLabel : UILabel?
    @IBOutlet var userPointLabel : UILabel?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
