//
//  LeaderboardViewController.swift
//  Piano
//
//  Created by Desap Team on 06/03/2018.
//  Copyright © 2018 Piano. All rights reserved.
//

import UIKit
import AudioToolbox
import AVKit

class LeaderboardViewController: UIViewController, GLNPianoViewDelegate,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableview : UITableView?
    var listOfLeaderBoard = NSArray()
    
    var soundIDs = [SystemSoundID]()
    @IBOutlet weak var keyboardView: GLNPianoView! {
        didSet{
            keyboardView.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        DispatchQueue.main.async {
            self.LoadPianoSound()
            
            self.tableview?.backgroundColor = UIColor.clear
        }
        
        self.GetLeaderBoardList()
    }
    func LoadPianoSound(){
        
        if let path = Bundle.main.path(forResource: "keyboardLayout", ofType: "plist") {
            
            //If your plist contain root as Array
            if let array = NSArray(contentsOfFile: path) {
                for (_, result) in array.enumerated() {
                    
                    if let pathStr = result as? String {
                        let sndurl = Bundle.main.url(forResource:pathStr, withExtension: "wav")!
                        var SoundID : SystemSoundID = 0
                        AudioServicesCreateSystemSoundID(sndurl as CFURL, &SoundID)
                        /*
                         // watch _this_ little move
                         AudioServicesAddSystemSoundCompletion(SoundID, nil, nil, {
                         sound, context in
                         print("finished!")
                         AudioServicesRemoveSystemSoundCompletion(sound)
                         AudioServicesDisposeSystemSoundID(sound)
                         }, nil)
                         */
                        self.soundIDs.append(SoundID)
                    }
                    
                }
            }
            
        }
        
        
        
    }
    
    //MARK:  -  Table View DataSource -
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.listOfLeaderBoard.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.listOfLeaderBoard.count > indexPath.row {
            if let leader = self.listOfLeaderBoard.object(at: indexPath.row) as? NSDictionary {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderBoardCell") as? LeaderBoardCell {
                    if let tempStr = leader.object(forKey: "name") as? String {
                        cell.usernameLabel?.text = tempStr
                    }
                    if let tempStr = leader.object(forKey: "point") as? String {
                        cell.userPointLabel?.text = tempStr
                    }
                    return cell
                }
            }
            
        }
        
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    func PlayHelpVideo(url : String){
        // Movie player
        if let videoUrl = URL(string: url) {
            let player = AVPlayer(url: videoUrl)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
        
    }
    //MARK:- Make API Call -
    func GetLeaderBoardList(){
        
        
//        var userId = ""
//        if let idStr = getUserAuthentication().object(forKey: "userID") as? String {
//            userId = idStr
//        }
        
        let requestUrl = String(format: "%@?view=leaderboard", arguments: [kRequestDomain])
        
        
        print(requestUrl)
        appDelegate.DesapShowLoader()
        request(requestUrl, headers: nil).responseJSON { response in
            print(response)
            appDelegate.DesapHideLoader()
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
                if let jsonRes = json as? NSDictionary {
                    if let statusCode = jsonRes.object(forKey: "msgcode") as? String {
                        if statusCode == "0" {
                            if let songs = jsonRes.object(forKey: "leaderboard_data") as? NSArray {
                                self.listOfLeaderBoard = NSArray(array: songs)
                                DispatchQueue.main.async {
                                    
                                    self.CollectionViewUpdate()
                                }
                            }
                            //
                            
                        }else{
                            if let message = jsonRes.object(forKey: "message") as? String {
                                appDelegate.TNMShowMessage("", message: message)
                                //                                JYToast().isShow(message)
                            }
                        }
                    }
                }else{
                    JYToast().isShow(kErrorMessage)
                }
            }else{
                JYToast().isShow(kErrorMessage)
            }
        }
    }
    func CollectionViewUpdate(){
        
        let nibName1 = UINib(nibName: "LeaderBoardCell", bundle: nil)
        self.tableview?.register(nibName1, forCellReuseIdentifier: "LeaderBoardCell")
        
        
        self.tableview?.dataSource = self
        self.tableview?.delegate = self
        self.tableview?.reloadData()
    }
    //MARK:- KeyBoard Delegate -
    func pianoKeyUp(_ keyNumber:Int) {
        print("pianoKeyUp")
        print(keyNumber)
        //        audioEngine.sampler.stopNote(UInt8(octave + keyNumber), onChannel: 0)
        
        keyboardView.stopPulse(keyNum: keyNumber)
        //        self.pulseNextStep(keyNumber: keyNumber)
    }
    //Manage Keyboard
    func pianoKeyDown(_ keyNumber:Int) {
        print("pianoKeyDown")
        print(keyNumber)
      
        
        if self.soundIDs.count > keyNumber {
            DispatchQueue.main.async {
                AudioServicesPlaySystemSound(self.soundIDs[keyNumber])
            }
            
        }
        
        
    }
    @IBAction func openMainMenu(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
