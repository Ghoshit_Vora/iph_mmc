//
//  TNMNavigationController.swift
//  ClearPay
//
//  Created by Desap Team on 26/09/2017.
//  Copyright © 2017 ClearPay. All rights reserved.
//

import UIKit

class TNMNavigationDefault: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationBar.barTintColor = NSTheme().GetNavigationBGColor()
        
        self.navigationBar.tintColor = NSTheme().GetNavigationTintColor()
        self.navigationBar.isTranslucent = true
        self.navigationBar.barStyle = .default
        
        let font = UIFont.boldSystemFont(ofSize: 22)
        self.navigationBar.titleTextAttributes = [ NSAttributedStringKey.font: font,NSAttributedStringKey.foregroundColor: NSTheme().GetNavigationTintColor()]
        
//        UIApplication.shared.statusBarView?.backgroundColor = .red
    }
    override func viewWillAppear(_ animated: Bool) {
//        UIApplication.shared.statusBarView?.backgroundColor = .red
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



