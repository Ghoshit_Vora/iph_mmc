//
//  PConstants.swift
//  Piano
//
//  Created by Desap Team on 06/01/2018.
//  Copyright © 2018 Piano. All rights reserved.
//

import Foundation

let kErrorMessage = "Our systems are currently unavailable. Please try again later. We apologise for any inconvenience."

let kAppName = "MakeMusicCount"
let kRequestDomain = "http://www.makemusiccount.online/app/index.php"



//Local user
func setUserAuthentication(_ data : NSDictionary){
    let dataStore = NSKeyedArchiver.archivedData(withRootObject: data)
    UserDefaults.standard.set(dataStore, forKey: "authentication")
    UserDefaults.standard.synchronize()
    
}

func getUserAuthentication() -> NSDictionary{
    var dict : NSDictionary = NSDictionary()
    if(UserDefaults.standard.object(forKey: "authentication") != nil ){
        let outData = UserDefaults.standard.data(forKey: "authentication")
        dict = (NSKeyedUnarchiver.unarchiveObject(with: outData!) as? NSDictionary)!
       
    }
    
    return dict
}

func removeUserAuthentication() {
    var dict : NSDictionary = NSDictionary()
    if(UserDefaults.standard.object(forKey: "authentication") != nil ){
        
        UserDefaults.standard.removeObject(forKey: "authentication")
    }
    
    
}

