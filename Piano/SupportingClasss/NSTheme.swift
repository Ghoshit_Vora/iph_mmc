//
//  NSTheme.swift
//  NovoNorDisk
//
//  Created by TNM3 on 6/23/15.
//  Copyright (c) 2015 TNM3. All rights reserved.
//

import UIKit

class NSTheme: NSObject {
    
    let kColorAlpha : CGFloat = 1.0
    
   
    func GetNavigationBGColor() -> UIColor {
//        return UIColor(red: 67/255, green: 74/255, blue: 75/255, alpha: kColorAlpha)
        return UIColor(red: 167/255, green: 192/255, blue: 64/255, alpha: kColorAlpha)
    }
    func GetNavigationTintColor() -> UIColor {
//        return UIColor.white
        return UIColor.white
//        return UIColor(red: 18/255, green: 159/255, blue: 223/255, alpha: kColorAlpha)
    }
    func GetThemeColor() -> UIColor {
        return UIColor(red: 72/255, green: 149/255, blue: 158/255, alpha: kColorAlpha)
    }
    func GetCustomGreyColor() -> UIColor {
        return UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: kColorAlpha)
        //        return UIColor(red: 18/255, green: 159/255, blue: 223/255, alpha: kColorAlpha)
        //        return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: kColorAlpha)
    }
    
    //Tababar
    func GetTabBarBGColor() -> UIColor {
        //return UIColor(red: 75/255, green: 154/255, blue: 161/255, alpha: kColorAlpha)
         return UIColor(red: 18/255, green: 141/255, blue: 154/255, alpha: kColorAlpha)
//        return UIColor(red: 41/255, green: 41/255, blue: 41/255, alpha: kColorAlpha)
//        return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: kColorAlpha)
    }
    
    
    func GetTabBarSelectedColor() -> UIColor {
        return UIColor(red: 10/255, green: 108/255, blue: 217/255, alpha: kColorAlpha)
//        return UIColor(red: 18/255, green: 159/255, blue: 223/255, alpha: kColorAlpha)
    }
    func GetTabBarUnSelectedColor() -> UIColor {
        return UIColor(red: 59/255, green: 63/255, blue: 68/255, alpha: kColorAlpha)
        //        return UIColor(red: 18/255, green: 159/255, blue: 223/255, alpha: kColorAlpha)
    }
    func GetTabBarNormalColor() -> UIColor {
        return UIColor.gray
    }
    
    func GetViewBGColor() -> UIColor {
        return UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: kColorAlpha)
    }
   
    func GetActiveSongBG() -> UIColor {
        return UIColor(red: 255/255, green: 188/255, blue: 20/255, alpha: kColorAlpha)
    }
    func GetSongButtonTextColor() -> UIColor {
        return UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: kColorAlpha)
    }
}
