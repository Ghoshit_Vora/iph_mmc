//
//  SongListCell.swift
//  Piano
//
//  Created by Desap Team on 08/12/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit

class CenterMenuCell: UICollectionViewCell {

    @IBOutlet var menuImage : UIImageView!
    @IBOutlet var menuLabel : UILabel!
    var data = NSDictionary()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
