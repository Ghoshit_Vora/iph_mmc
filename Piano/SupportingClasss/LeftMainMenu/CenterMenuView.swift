//
//  CenterMenuView.swift
//  Piano
//
//  Created by Desap Team on 16/01/2018.
//  Copyright © 2018 Piano. All rights reserved.
//

import UIKit
protocol CenterMenuDelegate {
    func didSelectItemAtIndex(index : Int, menu : String)
}

class CenterMenuView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    var delegate : CenterMenuDelegate?
    
    var listOfMenus = NSArray()
    var activeMenuName : String = ""
    @IBOutlet var collectionView : UICollectionView?
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> CenterMenuView {
        
        return UINib(nibName: "CenterMenuView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CenterMenuView
    }
    override func awakeFromNib() {
        self.registerCell()
    }
    func registerCell(){
        let nibName = UINib(nibName: "CenterMenuCell", bundle:nil)
        self.collectionView?.register(nibName, forCellWithReuseIdentifier: "CenterMenuCell")
        
        self.collectionView?.dataSource = self
        self.collectionView?.delegate = self
        self.collectionView?.reloadData()
    }
    //MARK: - UICollectionView Delegate -
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: (collectionView.bounds.size.width / 3), height: (collectionView.bounds.size.width / 3));
        
        return size
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listOfMenus.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CenterMenuCell", for: indexPath) as? CenterMenuCell {
            if self.listOfMenus.count > indexPath.row {
                if let result = self.listOfMenus.object(at: indexPath.row) as? NSDictionary {
                    if let titleStr = result.object(forKey: "title") as? String {
                        cell.menuLabel.text = titleStr
                    }
                }
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if self.delegate != nil {
            self.delegate?.didSelectItemAtIndex(index: indexPath.row, menu: self.activeMenuName)
        }
    }

}
