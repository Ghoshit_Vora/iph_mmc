//
//  LeftMenuCell.swift
//  Piano
//
//  Created by Desap Team on 11/11/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit

class LeftMenuCell: UITableViewCell {

    @IBOutlet var titleLabel : UILabel!
    @IBOutlet var menuIcon : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
