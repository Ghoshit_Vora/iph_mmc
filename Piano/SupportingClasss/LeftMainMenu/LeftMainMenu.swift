//
//  LeftMainMenu.swift
//  Piano
//
//  Created by Desap Team on 11/11/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import UIKit



protocol LeftMenuDelegate {
    func hideMenuPressed()
    func lessonStartIndex(index : Int)
    func didSelectMainMenu(menu : String)
}
class LeftMainMenu: UIView, UITableViewDelegate, UITableViewDataSource {

   
    @IBOutlet var userNameLabel : UILabel?
    @IBOutlet var userImage : UIImageView?
    
//    var delegate : LeftMenuDelegate?
    var delegate : LeftMenuDelegate?
    @IBOutlet var profileView : UIView!
    @IBOutlet var profileTitleView : UIView!
    
    @IBOutlet var tableview : UITableView!
    var NoOfCells = NSMutableArray()
    var mainMenuList : NSArray = ["my profile","subjects","songs","tutorial","login"]
    let tutorialList : NSArray = ["Half Steps","Whole Steps","Major Scales","Back"]
    let profileList : NSArray = ["leaderboard","my account","settings","back"]
    let subjectList : NSArray = ["Warm Up Songs","Substituting Variables","Solve for X","Creating Algebra Equations","Back"]
    
    var isActiveMenuTutorials : Bool = false
    var isActiveMenuSubject : Bool = false
    var isActiveMenuProfile : Bool = false
    
    class func instanceFromNib() -> LeftMainMenu {
       
        return UINib(nibName: "LeftMainMenu", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LeftMainMenu
    }
    override func awakeFromNib() {
        self.tableview.estimatedRowHeight = 60
        let nibName = UINib(nibName: "LeftMenuCell", bundle: nil)
        self.tableview .register(nibName, forCellReuseIdentifier: "LeftMenuCell")
        
//        self.tableview.backgroundColor = UIColor(red: 234/255, green: 166/255, blue: 33/255, alpha: 1)
        
        var isALreadyLogged : Bool = false
        if let username = getUserAuthentication().object(forKey: "userID") as? String {
            if username.isEmpty == false {
                isALreadyLogged = true
            }
        }
        if isALreadyLogged == true {
            mainMenuList = ["my profile","subjects","songs","tutorial","sign out"]
        }
        
        self.profileTitleView.isHidden = false
        self.tableview.isHidden = false
        self.profileView.isHidden = true
        
        
        self.CreateMoreCell()
    }
    @IBAction func ContinuesLesson(){
        self.profileTitleView.isHidden = false
        self.tableview.isHidden = false
        self.profileView.isHidden = true
        
        self.isActiveMenuTutorials = true
        self.CreateMoreCell()
    }
    
    func WhenProfileIsActive(){
        self.profileTitleView.isHidden = true
        self.tableview.isHidden = true
        self.profileView.isHidden = false
        
    }
    func CreateMoreCell(){
        
        print(getUserAuthentication())
        if let username = getUserAuthentication().object(forKey: "name") as? String {
            self.userNameLabel?.text = username
        }
        
        if let imageUrl = getUserAuthentication().object(forKey: "image") as? String {
            if let request = URL(string: imageUrl) {
                self.userImage?.af_setImage(withURL: request, placeholderImage: #imageLiteral(resourceName: "userDefault"), filter: nil, progress: nil, progressQueue: .main, imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: true, completion: { (response) in
                    
                    
                })
            }
            
        }
        
        self.NoOfCells = NSMutableArray()
        var menuLists = NSArray()
        
        if self.isActiveMenuTutorials == true {
            menuLists = NSArray(array: tutorialList)
        }else if self.isActiveMenuSubject == true {
            menuLists = NSArray(array: subjectList)
        }else if self.isActiveMenuProfile == true {
            menuLists = NSArray(array: profileList)
        }
            
        else{
            menuLists = NSArray(array: mainMenuList)
        }
        
        for data in menuLists {
            
            if let tempData = data as? String
            {
                if let cell = self.tableview.dequeueReusableCell(withIdentifier: "LeftMenuCell") as? LeftMenuCell
                {
//                    if tempData.caseInsensitiveCompare("My Profile") == .orderedSame {
////                        cell.menuIcon.image = #imageLiteral(resourceName: "user_ic")
//                    }else if tempData.caseInsensitiveCompare("Song List") == .orderedSame {
////                        cell.menuIcon.image = #imageLiteral(resourceName: "volume_ic")
//                    }else if tempData.caseInsensitiveCompare("Subject") == .orderedSame {
////                        cell.menuIcon.image = #imageLiteral(resourceName: "search_ic")
//                    }else {
////                        cell.menuIcon.image = #imageLiteral(resourceName: "blueCircle")
//                    }
                    cell.titleLabel.text = tempData
                    
                    self.NoOfCells.add(cell)
                }
            }
        }
        
       
        self.tableview.dataSource = self
        self.tableview.delegate = self
        self.tableview.reloadData()
    }
    
    //MARK:  -  Table View DataSource -
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.NoOfCells.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var values : CGFloat = 0
        if self.isActiveMenuTutorials == true {
            values = CGFloat(self.tutorialList.count)
        }else{
            values = CGFloat(self.mainMenuList.count)
        }
        return (UIScreen.main.bounds.size.height - 80) / values
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.NoOfCells.count > indexPath.row
        {
            if let cell = self.NoOfCells.object(at: indexPath.row) as? UITableViewCell {
                return cell
            }
            
        }
        return UITableViewCell()
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let cell = tableView.cellForRow(at: indexPath) as? LeftMenuCell {
            
            if let title = cell.titleLabel.text {
                
                if self.isActiveMenuTutorials == true {
                    if title.caseInsensitiveCompare("Back") == .orderedSame {
                        self.isActiveMenuTutorials = false
                        self.CreateMoreCell()
                        if self.delegate != nil {
                            self.delegate?.lessonStartIndex(index: 100)
                        }
                    }else {
                        if self.delegate != nil {
                            self.delegate?.lessonStartIndex(index: indexPath.row)
                        }
                    }
                }else if self.isActiveMenuSubject == true {
                    if title.caseInsensitiveCompare("Back") == .orderedSame {
                        self.isActiveMenuSubject = false
                        self.CreateMoreCell()
                        if self.delegate != nil {
                            self.delegate?.lessonStartIndex(index: 100)
                        }
                    }else {
                        if self.delegate != nil {
                            self.delegate?.lessonStartIndex(index: indexPath.row + 3)
                        }
                    }
                }else if self.isActiveMenuProfile == true {
                    if title.caseInsensitiveCompare("Back") == .orderedSame {
                        self.isActiveMenuProfile = false
                        self.CreateMoreCell()
                    }else if title.caseInsensitiveCompare("leaderboard") == .orderedSame {
                        if self.delegate != nil {
                            self.delegate?.didSelectMainMenu(menu: "leaderboard")
                        }
                        
                    }else if title.caseInsensitiveCompare("settings") == .orderedSame {
                        var isALreadyLogged : Bool = false
                        if let username = getUserAuthentication().object(forKey: "userID") as? String {
                            if username.isEmpty == false {
                                isALreadyLogged = true
                            }
                        }
                        if isALreadyLogged == true {
                            if self.delegate != nil {
                                self.delegate?.didSelectMainMenu(menu: "my profile")
                            }
                        }else{
                            self.isActiveMenuProfile = false
                            self.isActiveMenuTutorials = false
                            self.isActiveMenuSubject = false
                            if self.delegate != nil {
                                self.delegate?.didSelectMainMenu(menu: "sign out")
                            }
                        }
                        
                    }
                }
                else {
                    
                    if title.caseInsensitiveCompare("tutorial") == .orderedSame {
                        if self.delegate != nil {
                            self.delegate?.didSelectMainMenu(menu: "tutorial")
                        }
//                        self.isActiveMenuTutorials = true
//                        self.CreateMoreCell()
                    }else if title.caseInsensitiveCompare("subjects") == .orderedSame {
                        if self.delegate != nil {
                            self.delegate?.didSelectMainMenu(menu: "subjects")
                        }
//                        self.isActiveMenuTutorials = false
//                        self.isActiveMenuSubject = true
//                        self.CreateMoreCell()
                    }
                    else if title.caseInsensitiveCompare("my profile") == .orderedSame {
                        
                       self.isActiveMenuProfile = true
                        self.isActiveMenuTutorials = false
                        self.isActiveMenuSubject = false
                        self.CreateMoreCell()
//                        self.WhenProfileIsActive()
                    }else if title.caseInsensitiveCompare("songs") == .orderedSame {
                        self.isActiveMenuProfile = false
                        self.isActiveMenuTutorials = false
                        self.isActiveMenuSubject = false
                        if self.delegate != nil {
                            self.delegate?.didSelectMainMenu(menu: "songs")
                        }
                        
                    }else if title.caseInsensitiveCompare("sign out") == .orderedSame || title.caseInsensitiveCompare("login") == .orderedSame {
                        self.isActiveMenuProfile = false
                        self.isActiveMenuTutorials = false
                        self.isActiveMenuSubject = false
                        if self.delegate != nil {
                            self.delegate?.didSelectMainMenu(menu: "sign out")
                        }
                        
                    }
                    else{
                        if self.delegate != nil {
                            self.delegate?.didSelectMainMenu(menu: "")
                        }
                    }
                }
                
            }
        }
    }
    
    @IBAction func hideMenu(){
        if self.delegate != nil {
            self.delegate?.hideMenuPressed()
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
