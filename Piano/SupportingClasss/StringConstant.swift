//
//  StringConstant.swift
//  Piano
//
//  Created by Desap Team on 26/10/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import Foundation

let kLessonTypeIntro = "intro"
let kLessonTypeFormula = "formula"
let kLessonType = "lesson"


let kLessonDesc = "descption"
let kLessonButton = "button"
let kLessonFormula = "formula"
let kLessonAnswer = "answer"
let kLessonHint = "hint"


//Half Steps
let halfStepText1 = "A half step in music is when you play one note on the piano and then move up or down to the very next note to play."
let halfStepButton1 = "NEXT"

let halfStepText2 = "For example if you played the note C, a half step up would be C#, while a half step down from C would be the note B. On our musical number line we will represent a half step movement as 1/2. From this conclusion we see that the distance between each note on the Musical Number line is 1/2"
let halfStepButton2 = "TRY IT OUT!"

let halfStepText3 = "Find the note solves this half step equation."
let halfStepButton3 = "HINT"

let halfStepFormula = "G + 1/2 ="
let halfStepAnswere = "A♭"

let HalfStepHintKeyNumber = [8]


let halfStepsFinalResult : NSArray = [[kLessonDesc:halfStepText1,kLessonButton:halfStepButton1,kLessonType : kLessonTypeIntro],
[kLessonDesc:halfStepText2,kLessonButton:halfStepButton2,kLessonType : kLessonTypeIntro],
[kLessonDesc:halfStepText3,kLessonButton:halfStepButton3,kLessonType : kLessonTypeIntro],
[kLessonFormula:halfStepFormula,kLessonAnswer:halfStepAnswere,kLessonHint:HalfStepHintKeyNumber,kLessonType : kLessonTypeFormula]]

//Whole Steps
let wholeStepText1 = "In music there are also whole steps. A whole step is when you play one note and you skip the very next note to play the following note. A whole step is also two half steps from the original starting note."
let wholeStepButton1 = "NEXT"

let wholeStepText2 = "An example of a whole step is if you played the note F and then moved and played the note G. Notice that in order to move from F to G the note F# must be skipped. We will define whole steps as 1."
let wholeStepButton2 = "TRY IT OUT!"

let wholeStepText3 = "Find the note solves this whole step equation."
let wholeStepButton3 = "HINT"

let wholeStepFormula = "E + 1 ="
let wholeStepAnswere = "F#"

let wholeStepHintKeyNumber = [6]


let wholeStepsFinalResult : NSArray = [[kLessonDesc:wholeStepText1,kLessonButton:wholeStepButton1,kLessonType : kLessonTypeIntro],
                                      [kLessonDesc:wholeStepText2,kLessonButton:wholeStepButton2,kLessonType : kLessonTypeIntro],
                                      [kLessonDesc:wholeStepText3,kLessonButton:wholeStepButton3,kLessonType : kLessonTypeIntro],
                                      [kLessonFormula:wholeStepFormula,kLessonAnswer:wholeStepAnswere,kLessonHint:wholeStepHintKeyNumber,kLessonType : kLessonTypeFormula]]


//Majore Steps
let MajorStepText1 = "Now that we can see how math is used to show the distance between musical notes, we can use the same application for deriving musi-cal scales. Scales have a specific distance between each of the eight notes that make up that scale. With this knowledge we can derive any type of scale using math."
let MajorStepButton1 = "NEXT"

let MajorStepText2 = "Let’s take the major scales as an example. A major scale has eight notes, and when played it sounds ”happy.” Every note on the musi-cal number line can serve as the beginning of a major scale, which means that there are twelve major scales to learn as an aspiring pi-anist. But here is the good news, each major scale uses the same sequence of math distances regardless of what note it starts on.\nHere is the sequence to derive any major scale:\nMajor Scale = 1,1, 1/2, 1, 1, 1, 1/2\nThe numbers in the sequence represent the distance between each\nnote of the major scale. Let’s apply this sequence to derive the C\nMajor Scale."
let MajorStepButton2 = "TRY IT OUT!"

let MajorStepText3 = "Find the note solves this Major Scales steps equation."
let MajorStepButton3 = "HINT"

let MajorStepFormula = "C + 1 + 1 + 1/2 + 1 + 1 + 1 + 1/2 ="
let MajorStepAnswere = "C"

let MajorStepHintKeyNumber = [2,4,5,7,9,11,12]


let MajorStepsFinalResult : NSArray = [[kLessonDesc:MajorStepText1,kLessonButton:MajorStepButton1,kLessonType : kLessonTypeIntro],
                                       [kLessonDesc:MajorStepText2,kLessonButton:MajorStepButton2,kLessonType : kLessonTypeIntro],
                                       [kLessonDesc:MajorStepText3,kLessonButton:MajorStepButton3,kLessonType : kLessonTypeIntro],
                                       [kLessonFormula:MajorStepFormula,kLessonAnswer:MajorStepAnswere,kLessonHint:MajorStepHintKeyNumber,kLessonType : kLessonTypeFormula]]

//Warm Up Songs
let WarmUpStepText1 = "Warm Up Songs"
let WarmUpStepButton1 = "NEXT"

let WarmUpStepText2 = "An example of a Warm Up Songs is Rich Homie Quan - Flex:- F# + 1 ___ 1/2___ -4"
let WarmUpStepButton2 = "TRY IT OUT!"

let WarmUpStepText3 = "Let's practice"
let WarmUpStepButton3 = "START"

let WarmUpStepFormula = "F# + 1 ___ 1/2___ -4"
let WarmUpStepAnswere = "F#"

let WarmUpStepHintKeyNumber = NSArray()


let WarmUpStepsFinalResult : NSArray = [[kLessonDesc:WarmUpStepText1,kLessonButton:WarmUpStepButton1,kLessonType : kLessonTypeIntro],
                                       [kLessonDesc:WarmUpStepText2,kLessonButton:WarmUpStepButton2,kLessonType : kLessonTypeIntro],
                                       [kLessonDesc:WarmUpStepText3,kLessonButton:WarmUpStepButton3,kLessonType : kLessonTypeIntro],
                                       [kLessonFormula:WarmUpStepFormula,kLessonAnswer:WarmUpStepAnswere,kLessonHint:WarmUpStepHintKeyNumber,kLessonType : kLessonTypeFormula]
                                       ]

//Substituting Variables
let SubstitutingStepText1 = "Now that you understand the mathematics behind deriving your scales and playing some easy melodies of songs, you will now be able to play songs by applying your knowledge of Algebra. The songs in this section will focus on substituting variables and knowing how to properly apply the order of operations to derive the melodies and chords of songs."
let SubstitutingStepButton1 = "NEXT"



let SubstitutingStepText3 = "Let's practice"
let SubstitutingStepButton3 = "START"

let SubstitutingStepFormula = "2(x + 5)"
let SubstitutingStepAnswere = "F#"

let SubstitutingStepHintKeyNumber = NSArray()


let SubstitutingStepsFinalResult : NSArray = [[kLessonDesc:SubstitutingStepText1,kLessonButton:SubstitutingStepButton1,kLessonType : kLessonTypeIntro],
                                     [kLessonDesc:SubstitutingStepText3,kLessonButton:SubstitutingStepButton3,kLessonType : kLessonTypeIntro],
                                        [kLessonFormula:SubstitutingStepFormula,kLessonAnswer:SubstitutingStepAnswere,kLessonHint:SubstitutingStepHintKeyNumber,kLessonType : kLessonTypeFormula]
]

//Solve for X
let SolveforXText1 = "In this section we will focus on solving for x. Once all of the ques- tions are answered you will play them in columns forming the chords for each song. Remember that if you are playing a column of an- swers with your right hand the top note is played with your pinky finger, the middle note is played with your middle finger, and the bottom note is played with your thumb. Repeat this fingering for each column of the song."
let SolveforXButton1 = "NEXT"



let SolveforXText3 = "Let's practice"
let SolveforXButton3 = "START"

let SolveforXFormula = "B♭ + 2 = x"
let SolveforXAnswere = "F#"

let SolveforXHintKeyNumber = NSArray()


let SolveforXFinalResult : NSArray = [[kLessonDesc:SolveforXText1,kLessonButton:SolveforXButton1,kLessonType : kLessonTypeIntro],
                                              [kLessonDesc:SolveforXText3,kLessonButton:SolveforXButton3,kLessonType : kLessonTypeIntro],
                                              [kLessonFormula:SolveforXFormula,kLessonAnswer:SolveforXAnswere,kLessonHint:SolveforXHintKeyNumber,kLessonType : kLessonTypeFormula]
]

//Creating Algebra Equations
let AlgebraStepText1 = "In this workbook you’ve done a fantastic job of solving algebra equa-tions in order to get the correct musical notes to play on the piano. Now its time to take it a step further! You will now create your own algebra equations in order to equal the musical note given."
let AlgebraStepButton1 = "NEXT"

let AlgebraStepText2 = "Here are the guidelines that your algebra equation must follow:\n1. Pick a starting musical note\n2. Use both variables\n3. You must add or subtract\n4. You must multiply or divide\n5. Your equation must use parenthesis"
let AlgebraStepButton2 = "TRY IT OUT!"

let AlgebraStepText3 = "Let's practice"
let AlgebraStepButton3 = "START"

let AlgebraStepFormula = "E + (3x - 3y) = B♭"
let AlgebraStepAnswere = "F#"

let AlgebraStepHintKeyNumber = NSArray()


let AlgebraStepsFinalResult : NSArray = [[kLessonDesc:AlgebraStepText1,kLessonButton:AlgebraStepButton1,kLessonType : kLessonTypeIntro],
                                        [kLessonDesc:AlgebraStepText2,kLessonButton:AlgebraStepButton2,kLessonType : kLessonTypeIntro],
                                        [kLessonDesc:AlgebraStepText3,kLessonButton:AlgebraStepButton3,kLessonType : kLessonTypeIntro],
                                        [kLessonFormula:AlgebraStepFormula,kLessonAnswer:AlgebraStepAnswere,kLessonHint:AlgebraStepHintKeyNumber,kLessonType : kLessonTypeFormula]
]
/*



//Song 4 steps
let lessonSong4Title = "Creating Algebra Equations"
let lessonSong4Desc  = "In this workbook you’ve done a fantastic job of solving algebra equa-tions in order to get the correct musical notes to play on the piano. Now its time to take it a step further! You will now create your own algebra equations in order to equal the musical note given.\n\nHere are the guidelines that your algebra equation must follow:\n1. Pick a starting musical note\n2. Use both variables\n3. You must add or subtract\n4. You must multiply or divide\n5. Your equation must use parenthesis"

let lessonSong4FormulaTitle = "Let's practice!"
let lessonSong4FormulaDesc  = "E + (3x - 3y) = B♭"

*/


