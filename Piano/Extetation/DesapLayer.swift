//
//  DesapLayer.swift
//  Piano
//
//  Created by Desap Team on 25/10/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

import Foundation
import QuartzCore
import UIKit

open class DesapLayer: CALayer {
    var keyNums : Int = 0
    var keyNameABCD : String = ""
    var keyNameSharp : String = ""
    var nextStep : Int = 0
    var currentStepIndex : Int = 0
}
