//
//  TNMView.swift
//  ClearPay
//
//  Created by Desap Team on 09/10/2017.
//  Copyright © 2017 ClearPay. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable
open class TNMBlurView: UIVisualEffectView {
    
    @IBInspectable var borderColor : UIColor = UIColor.white {
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
        
    }
    @IBInspectable var borderWidth : CGFloat = 0.0 {
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var radius:CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = radius
        }
    }
}
