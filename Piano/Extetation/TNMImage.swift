//
//  TNMImage.swift
//  ClearPay
//
//  Created by Desap Team on 27/09/2017.
//  Copyright © 2017 ClearPay. All rights reserved.
//
import UIKit

extension UITableView {
    func reloadDataFade(){
        UIView.transition(with: self,
                          duration: 0.333,
                          options: [.transitionCrossDissolve],
                          animations: { () -> Void in
                            self.reloadData()
                            
        }, completion: nil)
    }
}
