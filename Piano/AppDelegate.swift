//
//  AppDelegate.swift
//  Piano
//
//  Created by Desap Team on 13/10/2017.
//  Copyright © 2017 Piano. All rights reserved.
//

//AppStore
//com.makemusic.count

//wildcard
//com.thedezine.*

import UIKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var navigationController = TNMNavigationDefault()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        var isALreadyLogged : Bool = false
        if let username = getUserAuthentication().object(forKey: "userID") as? String {
            if username.isEmpty == false {
                isALreadyLogged = true
            }
        }
        if isALreadyLogged == true {
            let controller = DashboardViewController(nibName: "DashboardViewController", bundle: nil)
            self.navigationController = TNMNavigationDefault(rootViewController: controller)
            self.window?.rootViewController = self.navigationController
            self.window?.makeKeyAndVisible()
        }else{
            let controller = LoginViewController(nibName: "LoginViewController", bundle: nil)
            self.navigationController = TNMNavigationDefault(rootViewController: controller)
            self.window?.rootViewController = self.navigationController
            self.window?.makeKeyAndVisible()
        }
        
        return true
    }

    //MARK: - TNM Error Alert -
    func TNMShowMessage(_ title : String, message : String){
        var titleStr = title
        if titleStr.isEmpty == true {
            titleStr = kAppName
        }
        
        let refreshAlert = UIAlertController(title: titleStr, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        //        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
        //
        //        }))
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            
            
        }))
        DispatchQueue.main.async {
            self.window?.visibleViewController?.present(refreshAlert, animated: true, completion: { () -> Void in
                
            })
            
        }
    }
    
    //MARK:- Loading Indicator & Messages
    //MARK:- TNM Loader -
    
    func DesapHideLoader(){
        
        SKActivityIndicator.dismiss()
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        //        if(UIApplication.shared.isIgnoringInteractionEvents){
        //            UIApplication.shared.endIgnoringInteractionEvents()
        //        }
        
        //        self.dialogBox.dismiss()
    }
    
    func DesapShowLoader(){
        
        SKActivityIndicator.spinnerColor(UIColor.darkGray)
        SKActivityIndicator.statusTextColor(UIColor.black)
        SKActivityIndicator.show("Loading...", userInteractionStatus: true)
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        //        UIApplication.shared.beginIgnoringInteractionEvents()
        
        //        self.dialogBox = DialogBox.show(title: "Please wait", message: "We are processing...", boxApp: BoxAppearanceLoading._7)
        
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

   

}
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

public extension UIWindow {
    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(self.rootViewController)
    }
    
    public static func getVisibleViewControllerFrom(_ vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(tc.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(pvc)
            } else {
                return vc
            }
        }
    }
}

